# SPDX-License-Identifier: MIT
require_relative "red_bird/red_bird"
require_relative "red_bird/controller"
require_relative "red_bird/dynamic_sprite"
require_relative "red_bird/engine"
require_relative "red_bird/entity"
require_relative "red_bird/input_device"
require_relative "red_bird/palette"
require_relative "red_bird/sprite"
require_relative "red_bird/version"

module RedBird
  class Error < StandardError; end
  # Your code goes here...
end
