# SPDX-License-Identifier: MIT
module RedBird
  # The InputDevice receives input from the keyboard, joystick, and mouse;
  # then, it translates the input into a meaningful command name and sends it
  # to a Controller. For example, you can use it to translate the 'space' key
  # from the keyboard and an 'x' button from a controller into a +:jump+
  # command.
  #
  # This class has an internal Hash that translates inputs into commands. For
  # every input, it looks if there is a key associated with that input; it
  # sends a command if such key exists; otherwise, it does nothing.
  #
  # @see RedBird::Controller
  # @author Frederico Linhares
  class InputDevice
    # Defines a {RedBird::Controller} to receives commands from the input.
    attr_writer :controller

    # @param controller [RedBird::Controller]
    # @author Frederico Linhares
    def initialize(controller)
      @controller = controller
      @keydown_inputs = {}
      @keyup_inputs = {}
    end

    # Associates a keydown even to a command_name.
    #
    # @param keycode [Integer] use constants under {RedBird::Keycode}.
    # @param command_name [Symbol]
    # @author Frederico Linhares
    def add_keydown(keycode, command_name)
      @keydown_inputs[keycode] = command_name
    end

    # Associates a keyup even to a command_name.
    #
    # @param keycode [Integer] use constants under {RedBird::Keycode}.
    # @param command_name [Symbol]
    # @author Frederico Linhares
    def add_keyup(keycode, command_name)
      @keyup_inputs[keycode] = command_name
    end

    # Gets all events that are in the queue, translate then into commands, and
    # send then to the current controller.
    #
    # @param entities [Array<RedBird::Entity>] entities that can receive events
    #   from mouse.
    # @author Frederico Linhares
    def exec_events(entities)
      events do |type, data|
        case type
        when :key_down then
          @controller.call_command(@keydown_inputs[data])
        when :key_up then
          @controller.call_command(@keyup_inputs[data])
        when :mouse_motion then
          entity = cursor_hover_entity(entities, data[:x], data[:y])
          entity.cursor_hover(data[:x], data[:y]) unless entity.nil?
        when :mouse_button_down then
          entity = cursor_hover_entity(entities, data[:x], data[:y])
          entity.cursor_down(data[:x], data[:y]) unless entity.nil?
        when :mouse_button_up then
          entity = cursor_hover_entity(entities, data[:x], data[:y])
          entity.cursor_up(data[:x], data[:y]) unless entity.nil?
        when :quit_game then
          Engine::quit_game
        end
      end
    end

    protected
    # This function is called when user move the mouse to se if mouse is hover
    # an entity.
    def cursor_hover_entity(entities, x, y)
      entities.each do |e|
        if(x >= e.pos_x and x <= e.pos_x + e.width and
           y >= e.pos_y and y <= e.pos_y + e.height) then
          return e
        end
      end
      return nil
    end
  end
end
