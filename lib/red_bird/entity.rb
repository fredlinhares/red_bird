# SPDX-License-Identifier: MIT
module RedBird
  # An entity is an object that is managed by the Engine; it has a size, a
  # position, ticks, and is renderable. {RedBird::Engine.run} expects every
  # single entity that it receives from {RedBird::Stage} to have these methods
  # and attributes.
  #
  # @author Frederico Linhares
  class Entity
    # Position
    #
    # @return [Integer]
    attr_reader :pos_x, :pos_y
    # Size
    #
    # @return [Integer]
    attr_reader :width, :height
    # Entities with higher priorit are called first when the
    # {RedBird::Engine.run} calls {#tick} and {#render}.
    #
    # @return [Integer]
    attr_reader :priority

    # The {RedBird::Engine.run} calls this method every single frame. Overwrite
    # it if you want your entity to do something every frame.
    #
    # @author Frederico Linhares
    def tick
      # By default do nothing
    end

    # The {RedBird::Engine.run} calls this method whenever the cursor is over
    # this entity. Overwrite this function if you want your entity to react to a
    # cursor hover.
    #
    # @param x [Integer] cursor position.
    # @param y [Integer] cursor position.
    # @author Frederico Linhares
    def cursor_hover(x, y)
      # By default do nothing
    end

    # The {RedBird::Engine.run} calls this method whenever there is a click over
    # this entity. Overwrite this function if you want your entity to react to a
    # click.
    #
    # @param x [Integer] cursor position.
    # @param y [Integer] cursor position.
    # @author Frederico Linhares
    def cursor_down(x, y)
      # By default do nothing
    end

    # The {RedBird::Engine.run} calls this method whenever there is a cursor
    # release over this entity. Overwrite this function if you want your entity
    # to react to a cursor release.
    #
    # @param x [Integer] cursor position.
    # @param y [Integer] cursor position.
    # @author Frederico Linhares
    def cursor_up(x, y)
      # By default do nothing
    end

    # The {RedBird::Engine.run} calls this method to render this entity into
    # the scree. Overwrite this method if you want to change how the entity
    # renders into the screen.
    #
    # @author Frederico Linhares
    def render
      self.current_sprite.render_to_screen(self.pos_x, self.pos_y)
    end
  end
end
