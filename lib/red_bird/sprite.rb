# SPDX-License-Identifier: MIT
module RedBird
  # A rectangular part of a texture. It allows you to print only a portion of
  # the image instead of it all. This will, for example, enable you to insert
  # several animation frames into the same texture instead of having to load
  # several small files.
  # @author Frederico Linhares
  class Sprite
    # Create an array of sprites and return it. The array has homogeneous
    # sprites that do not overlap and have no gaps between them.
    #
    # To create a grid  starting from the position x 20, y 20, containing three
    # columns and two rows, each sprite, having 100x80 pixels:
    #
    #   RedBird::Texture.grid sprite, 20, 20, 3, 2, 100, 80
    #
    # @param texture [RedBird::Texture] a texture that will be used as the
    #   source for all sprites.
    # @param init_x [Integer] initial x coordinate for the grid.
    # @param init_y [Integer] initial y coordinate for the grid.
    # @param num_hor_sprites [Integer] number of columns the grid will have.
    # @param num_ver_sprites [Integer] number of rows the grid will have.
    # @param width [Integer] width of every sprite.
    # @param height [Integer] height of every sprite.
    # @return [Array]
    # @author Frederico Linhares
    def self.grid(texture, init_x, init_y, num_hor_sprites, num_ver_sprites,
                  width, height)
      sprites = []
      num_ver_sprites.times do |v|
        num_hor_sprites.times do |h|
          sprites << Sprite.new(
            texture, init_x + h * width, init_y + v * height, width, height)
        end
      end

      return sprites
    end
  end
end
