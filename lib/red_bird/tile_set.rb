# SPDX-License-Identifier: MIT
require 'yaml'

module RedBird
  # A TileSet is a set of {RedBird::Sprite}s. It allows you to reuse the same
  # {RedBird::Sprite} several times in the creation of a game scenario, making
  # the level design more straightforward.
  #
  # @!attribute [r] tiles
  #   @return [Array<RedBird::TileSet::Sprite>]
  # @!attribute [r] tile_width
  #   @return [Integer] width of each tile.
  # @!attribute [r] tile_height
  #   @return [Integer] height of each tile.
  # @author Frederico Linhares
  class TileSet
    attr_reader :tiles, :tile_width, :tile_height

    # @param texture_file [String] this method uses the path as argument to
    #   create a {RedBird::Texture}.
    # @param tile_width [Integer] width of each tile.
    # @param tile_height [Integer] height of each tile.
    # @author Frederico Linhares
    def initialize(texture_file, texture_palette, tile_width, tile_height)
      @texture = RedBird::Texture.new(texture_file, texture_palette)

      @tile_width = tile_width
      @tile_height = tile_height

      horizontal_tiles = @texture.width / @tile_width
      vertical_tiles = @texture.height / @tile_height

      @tiles = Sprite.grid(
        @texture, 0, 0, horizontal_tiles, vertical_tiles, @tile_width,
        @tile_height)
    end

    # Load information from a yaml file and uses it create an instance of this
    # class.
    #
    # @param data_dir [String] path to the directory containing the yaml and
    #   other files referenced in the yaml.
    # @param tile_file [String] path to the yaml file.
    # @return [RedBird::TileSet]
    # @author Frederico Linhares
    def self.from_yml(data_dir, tile_file, palette)
      yaml_file = data_dir + tile_file

      data = YAML.load_file(yaml_file)
      texture_path = data_dir + data["texture"]

      return self.new(texture_path, palette, data["width"], data["height"])
    end

  end
end
