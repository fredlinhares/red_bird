# SPDX-License-Identifier: MIT
module RedBird
  # A Controller receives commands from a {RedBird::InputDevice} and executes
  # codes associated with those commands. Every {RedBird::InputDevice} instance
  # has a variable called +@controller+ that must point to an instance of this
  # class.
  #
  # The purpose of this class is to allow you to change the effect of user
  # input by merely changing the currently active controller; you would do so,
  # for example, when the user pauses the game, or when the user's character
  # enters a vehicle.
  #
  # @see RedBird::InputDevice
  # @author Frederico Linhares
  class Controller
    def initialize
      @commands = {}

      # By default, quit game when window is closed or another signal to end
      # the game is given.
      self.add_command(:quit_game) do
        Engine::quit_game
      end
    end

    # Use this method to create an association between a command and a code
    # block.
    #
    # @param command [Symbol]
    # @param function
    # @author Frederico Linhares
    def add_command(command, &function)
      @commands[command] = function
    end

    # Execute a code block associated with a command.
    #
    # @param command [Symbol]
    # @author Frederico Linhares
    def call_command(command)
      @commands[command].call unless @commands[command].nil?
    end
  end
end
