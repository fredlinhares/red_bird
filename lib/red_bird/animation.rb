# SPDX-License-Identifier: MIT
module RedBird
  # This module contains different kinds of animations.
  module Animation
    # Each frame has a sprite and a duration.
    #
    # @!attribute [r] duration
    #   @return [Integer] how long this frame lasts.
    # @!attribute [r] sprite
    #   @return [RedBird::Sprite] sprite to display in this frame.
    # @see RedBird::Animation::Base
    # @author Frederico Linhares
    class Frame
      attr_reader :duration, :sprite

      # @param duration [Integer] this animation frame will persist for as many
      #   game frames as defined by this value.
      # @param sprite [RedBird::Sprite] sprite to be displayed in this frame.
      # @author Frederico Linhares
      def initialize(duration, sprite)
        @duration = duration
        @sprite = sprite
      end

      # Create an array of frames and return it. This method must receive an
      # Array.
      #
      # @param frames [Array] each object in it must be an array containing the
      #   attributes for each frame returned.
      # @author Frederico Linhares
      def self.sequence(frames)
        return frames.collect { |i| Frame.new(i[0], i[1]) }
      end
    end

    # Base class for different animations. An animation consists of a sequence
    # of frames, a state to define which one to show and which comes next and
    # an animation method to determine how the frames change.
    #
    # @author Frederico Linhares
    class Base
      # @return [RedBird::Sprite] the sprite for the current stage of the
      #   animation.
      attr_reader :current_sprite

      # @param frames [Array] an array of RedBird::Frame.
      # @author Frederico Linhares
      def initialize(frames)
        @frames = frames
        self.set
      end

      # Set this animation to the initial state.
      #
      # @author Frederico Linhares
      def reset
        self.set
      end
    end

    # This animation starts in the first frame and goes through every frame
    # until the last, then it goes back to the first frame and starts again.
    #
    # @author Frederico Linhares
    class Loop < Base
      # This method must be called for every tick. It changes the current
      # displayed sprite.
      def animate
        @current_time += 1
        if @current_time > @frames[@current_frame].duration then
          @current_time -= @frames[@current_frame].duration
          @current_frame += 1
          if @current_frame >= @frames.size then
            @current_frame = 0
          end
          @current_sprite = @frames[@current_frame].sprite
        end
      end

      protected
      def set
        @current_time = 0
        @current_frame = 0
        @current_sprite = @frames[@current_frame].sprite
      end
    end

    # This animation starts in the first frame and goes through every frame
    # until the last; then, it moves backward through every frame and starts
    # again.
    #
    # @author Frederico Linhares
    class BackAndForth < Base
      # This method must be called for every tick. It changes the current
      # displayed sprite.
      def animate
        case @direction
        when :forward
          @current_time += 1
          if @current_time > @frames[@current_frame].duration then
            @current_time -= @frames[@current_frame].duration
            @current_frame += 1
            if @current_frame >= @frames.size then
              @direction = :backward
              @current_frame = @frames.size - 2
            end
            @current_sprite = @frames[@current_frame].sprite
          end
        when :backward
          @current_time += 1
          if @current_time > @frames[@current_frame].duration then
            @current_time -= @frames[@current_frame].duration
            @current_frame -= 1
            if @current_frame < 0 then
              @direction = :forward
              @current_frame = 1
            end
            @current_sprite = @frames[@current_frame].sprite
          end
        end
      end

      protected
      def set
        @direction = :forward
        @current_time = 0
        @current_frame = 0
        @current_sprite = @frames[@current_frame].sprite
      end
    end

  end
end
