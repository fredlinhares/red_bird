# SPDX-License-Identifier: MIT
require_relative 'entity'

module RedBird
  # The {RedBird::Entity} has an absolute position on the screen. Use this
  # class if you want an entity that has a position relative to a scenario
  # ({RedBird::TileMap}).
  #
  # @author Frederico Linhares
  class RelativeEntity < Entity
    # @return [RedBird::Rect]
    attr_reader :collision_box

    # @param relative_pos_x [Integer] position in the scenario
    # @param relative_pos_y [Integer] position in the scenario
    # @param width [Integer]
    # @param height [Integer]
    # @author Frederico Linhares
    def initialize(relative_pos_x, relative_pos_y, width, height)
      super()

      @priority = 1

      @collision_box = RedBird::Rect.new(
        relative_pos_x, relative_pos_y, width, height)
    end

    # Define which scenario to use as a reference for every
    # {RedBird::RelativeEntity}.
    #
    # @param scenario [RedBird::TileMap]
    # @author Frederico Linhares
    def self.scenario=(scenario)
      @@scenario = scenario
    end

    # @return [RedBird::TileMap]
    # @author Frederico Linhares
    def self.scenario
      return @@scenario
    end

    # @return [Integer] position in the scenario
    # @author Frederico Linhares
    def relative_pos_x
      return @collision_box.x
    end

    # @return [Integer] position in the scenario
    # @author Frederico Linhares
    def relative_pos_y
      return @collision_box.y
    end

    # @param x [Integer] position in the scenario
    # @author Frederico Linhares
    def relative_pos_x=(x)
      @collision_box.x = x
    end

    # @param y [Integer] position in the scenario
    # @author Frederico Linhares
    def relative_pos_y=(y)
      @collision_box.y = y
    end

    # Gets absolute position (position in the screen).
    #
    # @return [Float] position in the screen
    # @author Frederico Linhares
    def pos_x
      @collision_box.x - @@scenario.hor_offset + @@scenario.pos_x
    end

    # Gets absolute position (position in the screen).
    #
    # @return [Float] position in the screen
    # @author Frederico Linhares
    def pos_y
      @collision_box.y - @@scenario.ver_offset + @@scenario.pos_y
    end

    # @return [Integer]
    # @author Frederico Linhares
    def width
      @collision_box.width
    end

    # @return [Integer]
    # @author Frederico Linhares
    def height
      @collision_box.height
    end
  end
end
