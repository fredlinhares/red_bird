# SPDX-License-Identifier: MIT
module RedBird
  # This class does collision tests between entities.
  class EntityCollision
    # @param entities [Array<RedBird::Entity>] array containing the entities to
    #   test for collusion.
    # @author Frederico Linhares
    def initialize(entities)
      @entities = entities
    end

    # Loop over entities in the array and test if they collide. In the case of
    # collision, it calls the method +collide+ of the entity and sends the
    # entity it collided to as a single argument.
    #
    # @author Frederico Linhares
    def call
      # Calculate entities rote
      @entities.each_with_index do |e1, i|
        @entities.drop(i + 1).each do |e2|
          if e1.collision_box.collide? e2.collision_box then
            e1.collide(e2)
            e2.collide(e1)
            # If an object responds to 'collision_box' then it is tested.
          end if e1.respond_to?('collision_box')
        end
      end
    end

  end
end
