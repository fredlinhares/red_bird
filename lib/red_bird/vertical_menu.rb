# SPDX-License-Identifier: MIT
require_relative "uibox"

module RedBird
  # VerticalMenu is an interactive menu that receives input from players. It
  # consists of a vertical list of {RedBird::VerticalMenu::Option}.
  #
  # @author Frederico Linhares
  class VerticalMenu < Entity

    # An option to be used in {RedBird::VerticalMenu}.
    #
    # @!attribute [r] action
    #   @return [Proc] code for option selection
    # @!attribute [r] text
    #   @return [String] option name
    # @author Frederico Linhares
    class Option
      attr_accessor :action, :text

      # @param text [String] option name
      # @param action [Proc] code to use when this option is selected
      # @author Frederico Linhares
      def initialize(text, &action)
        @text = text
        @action = action
        @priority = 100
      end
    end

    # @param style [RedBird::UIBox::Style]
    # @param pos_x [Integer]
    # @param pos_y [Integer]
    # @param options [Array<RedBird::VerticalMenu::Option>]
    # @author Frederico Linhares
    def initialize(style, pos_x, pos_y, options)
      @style = style
      @pos_x = pos_x
      @pos_y = pos_y

      @options = options
      @current_option = 0
      @option_max_width = 0
      @option_max_height = 0

      @options.each do |i|
        if @option_max_width < i.text.width then
          @option_max_width = i.text.width
        end
        if @option_max_height < i.text.height then
          @option_max_height = i.text.height
        end
      end
    end

    # @return [Integer]
    # @author Frederico Linhares
    def width
      @option_max_width
    end

    # @return [Integer]
    # @author Frederico Linhares
    def height
      @option_max_height * @options.size
    end

    # Executes the action in the selected option.
    #
    # @author Frederico Linhares
    def activate
      @options[@current_option].action.call
    end

    # Changes the selected {RedBird::VerticalMenu::Option} to the next one.
    #
    # @author Frederico Linhares
    def next_opt
      @current_option += 1
      @current_option = 0 if @current_option >= @options.size
    end

    # Changes the selected {RedBird::VerticalMenu::Option} to the previous one.
    #
    # @author Frederico Linhares
    def pred_opt
      if @current_option <= 0 then
        @current_option = @options.size - 1
      else
        @current_option -= 1
      end
    end

    # Renders to the screen.
    #
    # @author Frederico Linhares
    def render
      # Render texts.
      @options.each_with_index do |o, i|
        o.text.render_to_screen(
          @style.sprite_width * 2 + @pos_x,
          @style.sprite_height * (i + 1) + @pos_y)
      end

      @style.sprites[:arrow_select].render_to_screen(
        @style.sprite_width + @pos_x,
        @style.sprite_height * (@current_option + 1) + @pos_y)
    end
  end
end
