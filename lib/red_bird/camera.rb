# SPDX-License-Identifier: MIT
module RedBird
  # Because most of the time the tilemap used as the scenario is larger than
  # the screen, you need to choose an entity to use as a reference. A Camera
  # control the scenario offset and makes the position of the scene rendered in
  # the screen be relative to an entity.
  #
  # @author Frederico Linhares
  class Camera

    # @param focus [RedBird::RelativeEntity] entity to use as reference for the
    #   scenario
    # @param scenario [RedBird::TileMap]
    # @author Frederico Linhares
    def initialize(focus, scenario)
      @scenario = scenario
      @max_hor_offset = @scenario.total_width - @scenario.width
      @max_ver_offset = @scenario.total_height - @scenario.height
      self.focus = focus
    end

    # Change the camere focus.
    #
    # @param focus [RedBird::RelativeEntity]
    # @author Frederico Linhares
    def focus=(focus)
      @focus = focus
      set_center
    end

    # Updates scenario offset according to entity position.
    #
    # @author Frederico Linhares
    def call
      hor_offset = (@focus.relative_pos_x - @hor_center).to_i
      if hor_offset < 0 then
        @scenario.hor_offset = 0
      elsif hor_offset > @max_hor_offset then
        @scenario.hor_offset = @max_hor_offset
      else
        @scenario.hor_offset = hor_offset
      end

      ver_offset = (@focus.relative_pos_y - @ver_center).to_i
      if ver_offset < 0 then
        @scenario.ver_offset = 0
      elsif ver_offset > @max_ver_offset then
        @scenario.ver_offset = @max_ver_offset
      else
        @scenario.ver_offset = ver_offset
      end
    end

    private

    def set_center
      @hor_center = @scenario.width / 2 - @focus.width / 2
      @ver_center = @scenario.height / 2 - @focus.height / 2
    end
  end
end
