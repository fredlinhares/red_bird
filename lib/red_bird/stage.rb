# SPDX-License-Identifier: MIT
module RedBird
  # A game built with RedBird consists of a succession of Stages. A game has
  # only a single Stage running at a time. Each Stage must have an
  # {RedBird::InputDevice} instance and an Array of {RedBird::Entity}
  # instances.
  #
  # This class contains a basic set of functionalities that every stage of a
  # game needs.
  #
  # @!attribute [r] input_device
  #   @return [RedBird::InputDevice] the class that inherits from Stage must
  #     defines an variable +@input_device+ containing and instance of
  #     {RedBird::InputDevice}.
  # @!attribute [r] entities
  #   @return [Array<RedBird::Entity>]
  # @author Frederico Linhares
  class Stage
    attr_accessor :input_device, :entities

    # @param global_data [Object] use this to share common data among different
    #   stages.
    # @author Frederico Linhares
    def initialize(global_data)
      @global_data = global_data
      @entities = []
    end

    # Add a new entity to this stage. This method organizes the entities as
    # expected by some methods in {RedBird::Engine}.
    #
    # @param entities [Entity, Array<Entity>]
    # @author Frederico Linhares
    def add_entities(entities)
      if entities.is_a? Array then
        @entities.concat entities
      else
        @entities << entities
      end

      @entities.sort! { |a, b| a.priority <=> b.priority }
    end

    # {RedBird::Engine} calls this method before it ticks entities. Overwrite
    # this to add routines that must run before entities tick.
    #
    # @author Frederico Linhares
    def pre_tick
      # By default do nothing.
    end

    # {RedBird::Engine} calls this method after it ticks entities. Overwrite
    # this to add routines that must run after entities tick.
    #
    # @author Frederico Linhares
    def post_tick
      # By default do nothing.
    end
  end
end
