# SPDX-License-Identifier: MIT
module RedBird
  # User Interface Box works as a visual background for texts, menus, or
  # anything else.
  #
  # @author Frederico Linhares
  class UIBox < Entity
    # A style defines the visual appearance of a Box.
    #
    # @author Frederico Linhares
    class Style
      # @!attribute [r] texture
      #   @return [RedBird::Texture]
      # @!attribute [r] bg_color
      #   @return [RedBird::Color]
      # @!attribute [r] sprites
      #   @return [Array<RedBird::Sprite>]
      # @!attribute [r] sprite_width
      #   @return [Integer]
      # @!attribute [r] sprite_height
      #   @return [Integer]
      attr_reader :texture, :bg_color, :sprites, :sprite_width, :sprite_height

      # @param texture_file [String] path to a image file in the format
      #   required by this class.
      # @param bg_color [RedBird::Color] color used to fill the background of
      #   the box.
      # @param sprite_width [Integer] width of each sprite in the
      #   +texture_file+.
      # @param sprite_height [Integer] height of each sprite in the
      #   +texture_file+.
      # @author Frederico Linhares
      def initialize(texture_file, texture_palette, bg_color, sprite_width,
                     sprite_height)
        @texture = Texture.new(texture_file, texture_palette)
        @bg_color = bg_color
        @sprite_width = sprite_width
        @sprite_height = sprite_height

        @sprites = {}
        @sprites[:arrow_select] = Sprite.new(
          @texture, 1 * @sprite_width, 1 * @sprite_height,
          @sprite_width, @sprite_height)
        @sprites[:box_top_left] = Sprite.new(
          @texture, 0, 0, @sprite_width, @sprite_height)
        @sprites[:box_top] = Sprite.new(
          @texture, 1 * @sprite_width, 0,
          @sprite_width, @sprite_height)
        @sprites[:box_top_right] = Sprite.new(
          @texture, 2 * @sprite_height, 0,
          @sprite_width, @sprite_height)
        @sprites[:box_left] = Sprite.new(
          @texture, 0, 1 * @sprite_height,
          @sprite_width, @sprite_height)
        @sprites[:box_right] = Sprite.new(
          @texture, 2 * @sprite_width, 1 * @sprite_height,
          @sprite_width, @sprite_height)
        @sprites[:box_bottom_left] = Sprite.new(
          @texture, 0, 2 * @sprite_height,
          @sprite_width, @sprite_height)
        @sprites[:box_bottom] = Sprite.new(
          @texture, 1 * @sprite_width, 2 * @sprite_height,
          @sprite_width, @sprite_height)
        @sprites[:box_bottom_right] = Sprite.new(
          @texture, 2 * @sprite_width, 2 * @sprite_height,
          @sprite_width, @sprite_height)
      end
    end

    # @param style [RedBird::Style]
    # @param pos_x [Integer]
    # @param pos_y [Integer]
    # @param num_vertical_sprites [Integer] number of sprites rows.
    # @param num_horizontal_sprites [Integer] number of sprites columns.
    # @author Frederico Linhares
    def initialize(style, pos_x, pos_y, num_vertical_sprites,
                   num_horizontal_sprites)
      @style = style
      @pos_x = pos_x
      @pos_y = pos_y
      @num_vertical_sprites = num_vertical_sprites
      @num_horizontal_sprites = num_horizontal_sprites
    end

    # The height is defined by +num_horizontal_sprites+ plus two (the borders).
    # The width of each sprite is defined by the style used.
    #
    # @return [Integer]
    # @author Frederico Linhares
    def width
      @style.sprite_width * (@num_horizontal_sprites + 2)
    end

    # The height is defined by +num_vertical_sprites+ plus two (the borders).
    # The height of each sprite is defined by the style used.
    #
    # @return [Integer]
    # @author Frederico Linhares
    def height
      @style.sprite_height * (@num_vertical_sprites + 2)
    end

    # Renders to the screen.
    #
    # @author Frederico Linhares
    def render
      Render.set_color(@style.bg_color)
      Render.fill_rect(@pos_x, @pos_y, self.width, self.height)

      # Draw the corners.
      @style.sprites[:box_top_left].render_to_screen(@pos_x, @pos_y)
      @style.sprites[:box_top_right].render_to_screen(
        @style.sprite_width * (@num_horizontal_sprites + 1) + @pos_x,
        @pos_y)
      @style.sprites[:box_bottom_left].render_to_screen(
        @pos_x,
        @style.sprite_height * (@num_vertical_sprites + 1) + @pos_y)
      @style.sprites[:box_bottom_right].render_to_screen(
        @style.sprite_width * (@num_horizontal_sprites + 1) + @pos_x,
        @style.sprite_height * (@num_vertical_sprites + 1) + @pos_y)

      # Draw the edges.
      @num_horizontal_sprites.times do |i|
        # Top
        @style.sprites[:box_top].render_to_screen(
          @style.sprite_width * (i + 1) + @pos_x, @pos_y)
        # Bottom
        @style.sprites[:box_bottom].render_to_screen(
          @style.sprite_width * (i + 1) + @pos_x,
          @style.sprite_height * (@num_vertical_sprites + 1) + @pos_y)
      end
      @num_vertical_sprites.times do |i|
        # Left
        @style.sprites[:box_left].render_to_screen(
          @pos_x, @style.sprite_height * (i + 1) + @pos_y)
        # Right
        @style.sprites[:box_right].render_to_screen(
          @style.sprite_width * (@num_horizontal_sprites + 1) + @pos_x,
          @style.sprite_height * (i + 1) + @pos_y)
      end
    end
  end
end
