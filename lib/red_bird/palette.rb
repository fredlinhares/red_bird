# SPDX-License-Identifier: MIT
require 'yaml'

module RedBird
  class Palette
    # Load information from a YAML file and uses it to create an instance of
    # this class
    #
    # @param tile_file [String] path to the yaml file.
    # @return [RedBird::Palette]
    # @author Frederico Linhares
    def self.from_yml(palette_file)
      data = YAML.load_file(palette_file)

      # Convert data from YAML into Color instances.
      colors = data.map do |i|
        Color.new(
          i[0, 2].to_i(16), i[2, 2].to_i(16), i[4, 2].to_i(16),
          i[6, 2].to_i(16))
      end

      return self.new(colors)
    end
  end
end
