# SPDX-License-Identifier: MIT
module RedBird
  module Engine
    # Maximum frames per second. The Engine will try to maintain the game
    # running at the frame rate defined by this variable.
    @@max_fps = 30

    # Set frames per second.
    #
    # @param fps [Integer]
    # @author Frederico Linhares
    def self.fps=(fps)
      @@max_fps = fps
    end

    # Get frames per second.
    #
    # @return [Integer]frames per second.
    def self.fps
      return @@max_fps
    end

    # Exit the current stage and end the {Engine#run} loop.
    #
    # @author Frederico Linhares
    def self.quit_game
      @@quit_game = true
      @@quit_stage = true
    end

    # Exit the current stage. The code block sent to {Engine#run} will be
    # called again to get the next stage.
    #
    # @author Frederico Linhares
    def self.quit_stage
      @@quit_stage = true
    end

    # This method initializes the engine, executes the main loop, and returns
    # when the game is over. This function calls the code block to get the
    # first stage of the game and call it again every time a stage end to get
    # the next one.
    #
    # @yield [global_data] gives +global_data+ to the block and expects it to
    #   return the next stage. The +global_data+ is an empty hash to store all
    #   data that must be preserved after the stages are over.
    # @author Frederico Linhares
    def self.run(&stage_manager)
      @@quit_game = false

      self.load do
        global_data = {}

        while not @@quit_game do
          @@quit_stage = false

          timer = Timer.new(@@max_fps)
          clear_color = Color.new(0, 0, 0, 0)
          current_stage = stage_manager.call(global_data)

          # The best moment to cleanup memory is before the engine loads a new
          # stage.
          GC.start

          while not @@quit_stage do
            timer.tick

            current_stage.input_device.exec_events(current_stage.entities)

            # Clear screen for render a new frame.
            Render.set_color(clear_color)
            Render.clear_screen

            # Execute actions for every entity in the stage.
            current_stage.pre_tick
            current_stage.entities.each { |e| e.tick }
            current_stage.post_tick

            # Renders entities and render current frame into the screen.
            current_stage.entities.each { |e| e.render }
            Render.update_screen
          end
        end
      end
    end
  end
end
