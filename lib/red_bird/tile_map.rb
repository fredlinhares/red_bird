# SPDX-License-Identifier: MIT
require_relative "tile_set"

module RedBird
  # A TileMap is a grid of {RedBird::Sprite}s; it works as a scenario. It has
  # scrolling functionalities to control which portion of it is on the screen.
  #
  # @!attribute [rw] hor_offset
  #   @return [Integer] how much the tilemap scrolled from the left corner.
  # @!attribute [rw] ver_offset
  #   @return [Integer] how much the tilemap scrolled from the top.
  # @!attribute [r] tiles
  #   @return [Array<Integer>] tiles indexes.
  # @!attribute [r] tile_set
  #   @return [RedBird::TileSet] tile set in use by this class.
  # @!attribute [r] total_width
  #   @return [Integer] width of the entire tilemap (not only what is displayed
  #     in the screen).
  # @!attribute [r] total_height
  #   @return [Integer] height of the entire tilemap (not only what is
  #     displayed in the screen).
  # @author Frederico Linhares
  class TileMap < Entity
    attr_accessor :hor_offset, :ver_offset
    attr_reader :tiles, :tile_set, :total_width, :total_height

    # @param tile_set [RedBird::TileSet]
    # @param pos_x [Integer] horizontal position of this map in the screen.
    # @param pos_y [Integer] vertical position of this map in the screen.
    # @param width [Integer] width of the region displayed in the screen.
    # @param height [Integer] height of the region displayed in the screen.
    # @param num_hor_tiles [Integer] total width in number of tiles.
    # @param num_ver_tiles [Integer] total height in number of tiles.
    # @param tiles [Array<Integer>] code of every tile to display, must be an
    #   one dimension array.
    # @author Frederico Linhares
    def initialize(tile_set, pos_x, pos_y, width, height, num_hor_tiles,
                   num_ver_tiles, tiles)
      @tile_set = tile_set
      @pos_x = pos_x
      @pos_y = pos_y

      @width = width
      @height = height

      @ver_offset = 0
      @hor_offset = 0

      @priority = -1

      @tiles = tiles

      # Number of tiles that are going to be rendered to screen.
      @render_hor_tiles = @width / @tile_set.tile_width + 1
      @render_ver_tiles = @height / @tile_set.tile_height + 1

      @total_hor_tiles = num_hor_tiles
      @total_ver_tiles = num_ver_tiles

      @total_width = @total_hor_tiles * @tile_set.tile_width
      @total_height = @total_ver_tiles * @tile_set.tile_height
    end

    # Load information from a yaml file and uses it create an instance of this
    # class.
    #
    # @param data_dir [String] path to the directory containing the yaml and
    #   other files referenced in the yaml.
    # @param map_file [String] path to the yaml file.
    # @return [RedBird::TileMap]
    # @author Frederico Linhares
    def self.from_yml(data_dir, map_file, tile_set, pos_x, pos_y, width,
                      height)
      yaml_file = data_dir + map_file

      data = YAML.load_file(yaml_file)
      num_hor_tiles = data["num_hor_tiles"]
      num_ver_tiles = data["num_ver_tiles"]
      tiles = data["tiles"].unpack("S*")

      return self.new(
               tile_set, pos_x, pos_y, width, height, num_hor_tiles,
               num_ver_tiles, tiles)
    end

    # Gets the {RedBird::Sprite} at position x, y.
    #
    # @param x [Integer]
    # @param y [Integer]
    # @author Frederico Linhares
    def tile_xy(x, y)
      return @tile_set.tiles[@tiles[y_to_tile(y) + x]][:type]
    end

    # Renders this tilemap to the screen.
    #
    # @author Frederico Linhares
    def render
      first_hor_tile = @hor_offset / @tile_set.tile_width
      first_ver_tile = @ver_offset / @tile_set.tile_height

      (first_ver_tile..(first_ver_tile + @render_ver_tiles)).each do |y|
        (first_hor_tile..(first_hor_tile + @render_hor_tiles)).each do |x|
          unless @tiles[y_to_tile(y) + x].nil? then
            @tile_set.tiles[@tiles[y_to_tile(y) + x]].render_to_screen(
              x * @tile_set.tile_width - @hor_offset + @pos_x,
              y * @tile_set.tile_height - @ver_offset + @pos_y)
          end
        end
      end
    end

    private
    def y_to_tile(y)
      return y * @total_hor_tiles
    end
  end
end
