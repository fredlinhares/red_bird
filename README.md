# RedBird

RedBird is a 2D game engine for Ruby, it is designed to make game development quick and easy.

## Installation

RedBird gem links against libSDL2 and libSDL2_ttf, so before installing RedBird into a machine, ensure that development libraries for SDL are installed.

Add this line to your application's Gemfile:

```ruby
gem 'red_bird'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install red_bird

## Usage

To crate a minimal code using RedBird you need to call {RedBird::Engine.run}
and pass to it a code block that returns a {RedBird::Stage}:

    require 'red_bird'
    require 'red_bird/stage'

    class Stage < RedBird::Stage
      def initialize(global_data)
	    super(global_data)
        @controller = RedBird::Controller.new
        @input_device = RedBird::InputDevice.new(@controller)
      end
    end

    RedBird::Engine.run(nil) { |global_data| Stage.new(global_data) }

## Contributing

Bug reports and pull requests are welcome on GitHub at [bitbucket.org/fredlinhares/red_bird](https://bitbucket.org/fredlinhares/red_bird)

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
