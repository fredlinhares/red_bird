// SPDX-License-Identifier: MIT
#include "timer.h"

#include "engine.h"

/*
  Document-class: RedBird::Timer

  A Timer controls the speed of execution of a frame; it prevents that a frame
  run too quickly and give you compensation when a frame runs too slow.

  When creating a new instance, you must define the maximum frame rate that you
  want; then, you should call {#tick} at every frame to get a delta time. The
  delta time is proportional to the frame rate defined during the creation of
  the instance or is higher when the last frame was slower than expected.

  @author Frederico Linhares
*/
VALUE bird_cTimer;

/*
  Basic functions all Ruby classes need.
 */

static void
bird_free_timer(void* obj)
{
  struct bird_timer_data *ptr = obj;

  free(ptr);
}

static size_t
bird_memsize_timer(const void* obj)
{
  // TODO
  return 0;
}

static const rb_data_type_t
bird_timer_type = {
  "red_bird_timer",
  {0, bird_free_timer, bird_memsize_timer,},
  0, 0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

static VALUE
bird_alloc_timer(VALUE klass)
{
  VALUE obj;
  struct bird_timer_data *ptr;

  obj = TypedData_Make_Struct(klass, struct bird_timer_data, &bird_timer_type,
                              ptr);

  return obj;
}

/*
  @param max_fps [Integer] the maximum of frames to be rendered per second.
  @author Frederico Linhares
*/
VALUE
bird_cTimer_initialize(VALUE self, VALUE max_fps)
{
  struct bird_timer_data *ptr;

  if(!engine_initialized)
    rb_raise(rb_eRuntimeError, "%s",
             "can not create a RedBird::Timer instance before "
             "RedBird::Engine is started");

  RB_INTEGER_TYPE_P(max_fps);

  TypedData_Get_Struct(self, struct bird_timer_data, &bird_timer_type, ptr);
  ptr->max_frame_duration_ms = 1000 / NUM2INT(max_fps);
  ptr->max_frame_duration_sec = ptr->max_frame_duration_ms/1000.0;
  ptr->frame_start = SDL_GetTicks();

  return self;
}

/*
  Every time you call this method, it compares the current time with the last
  time it was called; if the amount of time is inferior to the frame duration,
  it waits to ensure the frame has the exact duration expected; otherwise, it
  returns instantly and gives you a delta value that compensates for the extra
  duration of the last frame.

  @return [Float] represents the amount of time between this frame and the last
    frame, the higher, the higher amount of time has passed.
  @author Frederico Linhares
*/
VALUE
bird_cTimer_tick(VALUE self)
{
  struct bird_timer_data *ptr;
  Uint32 frame_stop;
  Uint32 frame_duration;
  VALUE delta;

  TypedData_Get_Struct(self, struct bird_timer_data, &bird_timer_type, ptr);

  frame_stop = SDL_GetTicks();
  frame_duration = frame_stop - ptr->frame_start;

  // If frame take less time than maximum allowed.
  if(ptr->max_frame_duration_ms > frame_duration)
  {
    SDL_Delay(ptr->max_frame_duration_ms - frame_duration);
    delta = rb_float_new(ptr->max_frame_duration_sec);
  }
  // If frame take too long time.
  else
  {
    // SDL_GetTicks return time im miliseconds, so I need to divide by 1000 to
    // get the time in seconds.
    delta = rb_float_new((double)frame_duration/1000.0);
  }

  ptr->frame_start = frame_stop;

  return delta;
}

void
Init_red_bird_timer(void)
{
  bird_cTimer = rb_define_class_under(bird_m, "Timer", rb_cObject);
  rb_define_alloc_func(bird_cTimer, bird_alloc_timer);
  rb_define_method(bird_cTimer, "initialize", bird_cTimer_initialize, 1);
  rb_define_method(bird_cTimer, "tick", bird_cTimer_tick, 0);
}
