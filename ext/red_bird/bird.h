// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_BIRD_H
#define RED_BIRD_BIRD_H 1

#include "main.h"

void
Init_red_bird_module(void);

#endif /* RED_BIRD_MAIN_H */
