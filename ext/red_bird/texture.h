// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_TEXTURE_H
#define RED_BIRD_TEXTURE_H 1

#include "main.h"

extern VALUE bird_cTexture;

struct bird_texture_data
{
  SDL_Texture *data;
  int width, height;
};

VALUE
bird_alloc_texture(VALUE klass);

VALUE
bird_cTexture_initialize(VALUE self, VALUE file_path, VALUE palette);

VALUE
bird_cTexture_width(VALUE self);

VALUE
bird_cTexture_height(VALUE self);

struct bird_texture_data*
bird_cTexture_get_data(VALUE self);

void
Init_red_bird_texture(void);

#endif /* RED_BIRD_TEXTURE_H */
