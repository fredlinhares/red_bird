// SPDX-License-Identifier: MIT
#include "bird.h"

/*
  Document-module: RedBird

  This module exist to prevent name clashes.

  @author Frederico Linhares
 */
void
Init_red_bird_module(void)
{
  bird_m = rb_define_module("RedBird");
}
