// SPDX-License-Identifier: MIT
#include "color.h"
#include "palette.h"

/*
  Document-class: RedBird::Palette
  A palette consists in a set of 256 colors.

  @author Frederico Linhares
*/
VALUE bird_cPalette;

void
bird_free_palette(void* obj)
{
  struct bird_palette_data *ptr = obj;

  free(ptr);
}

size_t
bird_memsize_palette(const void* obj)
{
  // TODO
  return 0;
}

static const rb_data_type_t
bird_palette_type = {
  "red_bird_palette",
  {0, bird_free_palette, bird_memsize_palette,},
  0, 0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

VALUE
bird_alloc_palette(VALUE klass)
{
  VALUE obj;
  struct bird_palette_data *ptr;

  obj = TypedData_Make_Struct(klass, struct bird_palette_data,
                              &bird_palette_type, ptr);

  return obj;
}

/*
  @param colors [Array<RedBird::Color>] an Array with 256 instances of
    {RedBird::Color}.
  @author Frederico Linhares
 */
VALUE
bird_cPalette_initialize(VALUE self, VALUE colors)
{
  long ary_index, ary_len;
  VALUE v_color;
  struct bird_palette_data *ptr;
  const char* error_message =
      "colors must be an Array with 256 instances of RedBird::Color";

  if(!rb_obj_is_kind_of(colors, rb_cArray))
    rb_raise(rb_eArgError, "%s", error_message);

  ary_len = RARRAY_LEN(colors);
  if(ary_len != 256)
    rb_raise(rb_eArgError, "%s", error_message);

  TypedData_Get_Struct(self, struct bird_palette_data, &bird_palette_type,
                       ptr);

  for(ary_index = 0; ary_index < ary_len; ary_index++)
  {
    v_color = rb_ary_entry(colors, ary_index);
    if(!rb_obj_is_kind_of(v_color, bird_cColor))
      rb_raise(rb_eArgError, "%s", error_message);

    ptr->colors[ary_index] = bird_cColor_get_data(v_color)->data;
  }

  return self;
}

/*
  @param index [Integer] color index. Must be between 0 and 255.
  @return [RedBird::Color] a instance of {RedBird::Color} with values
    referenced by the index.
  @author Frederico Linhares
 */
VALUE
bird_cPalette_get_color(VALUE self, VALUE index)
{
  VALUE argv[4];
  int c_index;
  struct bird_palette_data *ptr;

  RB_INTEGER_TYPE_P(index);

  c_index = NUM2INT(index);
  if(c_index < 0 || c_index > 255)
    rb_raise(rb_eArgError, "%s", "index must be between 0 and 255");

  TypedData_Get_Struct(self, struct bird_palette_data, &bird_palette_type,
                       ptr);

  argv[0] = UINT2NUM(ptr->colors[c_index].r);
  argv[1] = UINT2NUM(ptr->colors[c_index].g);
  argv[2] = UINT2NUM(ptr->colors[c_index].b);
  argv[3] = UINT2NUM(ptr->colors[c_index].a);

  return rb_class_new_instance(4, argv, bird_cColor);
}

struct bird_palette_data*
bird_cPalette_get_data(VALUE self)
{
  struct bird_palette_data *ptr;

  TypedData_Get_Struct(self, struct bird_palette_data, &bird_palette_type,
                       ptr);

  return ptr;
}

void
Init_red_bird_palette(void)
{
  bird_cPalette = rb_define_class_under(bird_m, "Palette", rb_cObject);
  rb_define_alloc_func(bird_cPalette, bird_alloc_palette);
  rb_define_method(bird_cPalette, "initialize", bird_cPalette_initialize, 1);
  rb_define_method(bird_cPalette, "color", bird_cPalette_get_color, 1);
}
