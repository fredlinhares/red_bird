// SPDX-License-Identifier: MIT
#include "rect.h"

/*
  Document-class: RedBird::Rect

  An abstract rectangular object used mainly for collision detection.

  @author Frederico Linhares
*/
VALUE bird_cRect;

/*
  Basic functions all Ruby classes need.
 */

static void
bird_free_rect(void* obj)
{
  struct bird_rect_data *ptr = obj;

  free(ptr);
}

static size_t
bird_memsize_rect(const void* obj)
{
  // TODO
  return 0;
}

static const rb_data_type_t
bird_rect_type = {
  "red_bird_rect",
  {0, bird_free_rect, bird_memsize_rect,},
  0, 0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

static VALUE
bird_alloc_rect(VALUE klass)
{
  VALUE obj;
  struct bird_rect_data *ptr;

  obj = TypedData_Make_Struct(klass, struct bird_rect_data, &bird_rect_type,
                              ptr);

  return obj;
}

/*
  @param x [Integer] coordinate x for the rectangle position.
  @param y [Integer] coordinate y for the rectangle position.
  @param width [Integer] rectangle width.
  @param height [Integer] rectangle height.
  @author Frederico Linhares
*/
VALUE
bird_cRect_initialize(VALUE self, VALUE x, VALUE y, VALUE width, VALUE height)
{
  struct bird_rect_data *ptr;

  RB_INTEGER_TYPE_P(x);
  RB_INTEGER_TYPE_P(y);
  RB_INTEGER_TYPE_P(width);
  RB_INTEGER_TYPE_P(height);

  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr);

  ptr->data.x = NUM2INT(x);
  ptr->data.y = NUM2INT(y);
  ptr->data.w = NUM2INT(width);
  ptr->data.h = NUM2INT(height);

  return self;
}

/*
  @return [Integer] coordinate x of this rectangle.
  @author Frederico Linhares
*/
VALUE
bird_cRect_get_x(VALUE self)
{
  struct bird_rect_data *ptr;

  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr);

  return INT2FIX(ptr->data.x);
}

/*
  @return [Integer] coordinate y of this rectangle.
  @author Frederico Linhares
*/
VALUE
bird_cRect_get_y(VALUE self)
{
  struct bird_rect_data *ptr;

  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr);

  return INT2FIX(ptr->data.y);
}

/*
  Set the coordinate x of this rectangle.

  @param x [Integer]
  @author Frederico Linhares
*/
VALUE
bird_cRect_set_x(VALUE self, VALUE x)
{
  struct bird_rect_data *ptr;

  RB_INTEGER_TYPE_P(x);
  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr);

  ptr->data.x = FIX2INT(x);

  return x;
}

/*
  Set the coordinate y of this rectangle.

  @param y [Integer]
  @author Frederico Linhares
*/
VALUE
bird_cRect_set_y(VALUE self, VALUE y)
{
  struct bird_rect_data *ptr;

  RB_INTEGER_TYPE_P(y);
  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr);

  ptr->data.y = FIX2INT(y);

  return y;
}

/*
  @return [Integer] width of this rectangle.
  @author Frederico Linhares
*/
VALUE
bird_cRect_get_width(VALUE self)
{
  struct bird_rect_data *ptr;

  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr);

  return INT2FIX(ptr->data.w);
}

/*
  @return [Integer] height of this rectangle.
  @author Frederico Linhares
*/
VALUE
bird_cRect_get_height(VALUE self)
{
  struct bird_rect_data *ptr;

  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr);

  return INT2FIX(ptr->data.h);
}

/*
  Set the width of this rectangle.

  @param width [Integer]
  @author Frederico Linhares
*/
VALUE
bird_cRect_set_width(VALUE self, VALUE width)
{
  struct bird_rect_data *ptr;

  RB_INTEGER_TYPE_P(width);
  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr);

  ptr->data.w = FIX2INT(width);

  return width;
}

/*
  Set the height of this rectangle.

  @param height [Integer]
  @author Frederico Linhares
*/
VALUE
bird_cRect_set_height(VALUE self, VALUE height)
{
  struct bird_rect_data *ptr;

  RB_INTEGER_TYPE_P(height);
  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr);

  ptr->data.h = FIX2INT(height);

  return height;
}

/*
  Test if this rectangle collided with another rectangle.

  @param rect [RedBird::Rect]
  @return [Boolean] true if there is a collision, false if there is no
    collision.
  @author Frederico Linhares
*/
VALUE
bird_cRect_collide(VALUE self, VALUE rect)
{
  struct bird_rect_data *ptr1, *ptr2;

  if(!rb_obj_is_kind_of(rect, bird_cRect))
    rb_raise(rb_eTypeError, "%s",
             "rect must be an instance of RedBird::Rect");

  TypedData_Get_Struct(self, struct bird_rect_data, &bird_rect_type, ptr1);
  TypedData_Get_Struct(rect, struct bird_rect_data, &bird_rect_type, ptr2);

  if(ptr1->data.x <= ptr2->data.x + ptr2->data.w &&
     ptr1->data.x + ptr1->data.w >= ptr2->data.x &&
     ptr1->data.y <= ptr2->data.y + ptr2->data.h &&
     ptr1->data.y + ptr1->data.h >= ptr2->data.y)
    return Qtrue;
  else
    return Qfalse;
}

void
Init_red_bird_rect(void)
{
  bird_cRect = rb_define_class_under(bird_m, "Rect", rb_cObject);
  rb_define_alloc_func(bird_cRect, bird_alloc_rect);
  rb_define_method(bird_cRect, "initialize", bird_cRect_initialize, 4);

  rb_define_method(bird_cRect, "x", bird_cRect_get_x, 0);
  rb_define_method(bird_cRect, "y", bird_cRect_get_y, 0);
  rb_define_method(bird_cRect, "x=", bird_cRect_set_x, 1);
  rb_define_method(bird_cRect, "y=", bird_cRect_set_y, 1);
  rb_define_method(bird_cRect, "width", bird_cRect_get_width, 0);
  rb_define_method(bird_cRect, "height", bird_cRect_get_height, 0);
  rb_define_method(bird_cRect, "width=", bird_cRect_set_width, 1);
  rb_define_method(bird_cRect, "height=", bird_cRect_set_height, 1);

  rb_define_method(bird_cRect, "collide?", bird_cRect_collide, 1);
}
