// SPDX-License-Identifier: MIT
#include "text.h"

#include "color.h"
#include "engine.h"
#include "font.h"

/*
  Document-class: RedBird::Text

  A text to be printed on the screen. This class renders the text received
  during the initialization as an image and store the image internally for
  rendering.

  @author Frederico Linhares
*/
VALUE bird_cText;

/*
  Basic functions all Ruby classes need.
*/

static void
bird_free_text(void* obj)
{
  struct bird_text_data *ptr = obj;

  SDL_DestroyTexture(ptr->data);
  free(ptr);
}

static size_t
bird_memsize_text(const void* obj)
{
  // TODO
  return 0;
}

static const rb_data_type_t
bird_text_type = {
  "red_bird_text",
  {0, bird_free_text, bird_memsize_text,},
  0, 0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

static VALUE
bird_alloc_text(VALUE klass)
{
  VALUE obj;
  struct bird_text_data *ptr;

  obj = TypedData_Make_Struct(klass, struct bird_text_data, &bird_text_type,
                              ptr);

  return obj;
}

/*
  @param text [String] text to rendered.
  @param font [RedBird::Font] font to be used for rendering the text.
  @param tx_color [RedBird::Color] text color.
  @param bg_color [RedBird::Color, nil] if the attribute is a color, this color
    will be used as background; if the attribute is nil, there will be no
    background.
  @author Frederico Linhares
*/
VALUE
bird_cText_initialize(VALUE self, VALUE text, VALUE font, VALUE tx_color,
                      VALUE bg_color)
{
  SDL_Surface *surface = NULL;

  struct bird_text_data *ptr;
  struct bird_font_data *font_ptr;
  struct bird_color_data *tx_color_ptr;
  struct bird_color_data *bg_color_ptr;

  if(!engine_initialized)
    rb_raise(rb_eRuntimeError, "%s",
             "can not create a RedBird::Text instance before RedBird::Engine "
             "is started");

  SafeStringValue(text);

  if(!rb_obj_is_kind_of(font, bird_cFont))
  {
    rb_raise(rb_eTypeError, "%s", "font must be an instance of RedBird::Font");
    return self;
  }

  if(!rb_obj_is_kind_of(tx_color, bird_cColor))
  {
    rb_raise(rb_eTypeError, "%s",
             "tx_color must be an instance of RedBird::Color");
    return self;
  }

  font_ptr = bird_cFont_get_data(font);
  tx_color_ptr = bird_cColor_get_data(tx_color);
  if(TYPE(bg_color) == T_NIL)
  {
    surface = TTF_RenderUTF8_Solid(font_ptr->data, StringValueCStr(text),
                                   tx_color_ptr->data);
  }
  else if(rb_obj_is_kind_of(bg_color, bird_cColor))
  {
    bg_color_ptr = bird_cColor_get_data(bg_color);
    surface = TTF_RenderUTF8_Shaded(font_ptr->data, StringValueCStr(text),
                                    tx_color_ptr->data, bg_color_ptr->data);
  }
  else
  {
    rb_raise(rb_eTypeError, "%s",
             "bg_color must be an instance of RedBird::Color or nil");
    return self;
  }

  if(surface == NULL)
  {
    rb_raise(rb_eRuntimeError, "failed to render text: %s", TTF_GetError());
    return self;
  }

  TypedData_Get_Struct(self, struct bird_text_data, &bird_text_type, ptr);

  ptr->data = NULL;
  ptr->data = SDL_CreateTextureFromSurface(bird_core.renderer, surface);
  SDL_FreeSurface(surface);
  if(ptr->data == NULL)
  {
    rb_raise(rb_eRuntimeError, "failed to convert text: %s", SDL_GetError());
    return self;
  }

  SDL_QueryTexture(ptr->data, NULL, NULL, &ptr->width, &ptr->height);
  return self;
}

/*
  Returns the width of the image.

  @return [Integer] text width.
  @author Frederico Linhares
*/
VALUE
bird_cText_width(VALUE self)
{
  struct bird_text_data *ptr;

  TypedData_Get_Struct(self, struct bird_text_data, &bird_text_type, ptr);

  return INT2FIX(ptr->width);
}

/*
  Returns the height of the image.

  @return [Integer] text height.
  @author Frederico Linhares
*/
VALUE
bird_cText_height(VALUE self)
{
  struct bird_text_data *ptr;

  TypedData_Get_Struct(self, struct bird_text_data, &bird_text_type, ptr);

  return INT2FIX(ptr->height);
}

/*
  Render this text to the screen.

  @param x [Integer] screen horizontal position.
  @param y [Integer] screen vertical position.
  @return [RedBird::Sprite] self
  @author Frederico Linhares
*/
VALUE
bird_cText_render_to_screen(VALUE self, VALUE  x, VALUE  y)
{
  SDL_Rect dst_rect;

  struct bird_text_data *ptr;

  RB_INTEGER_TYPE_P(x);
  RB_INTEGER_TYPE_P(y);

  TypedData_Get_Struct(self, struct bird_text_data, &bird_text_type, ptr);

  dst_rect.x = NUM2INT(x);
  dst_rect.y = NUM2INT(y);
  dst_rect.w = ptr->width;
  dst_rect.h = ptr->height;

  SDL_RenderCopy(bird_core.renderer, ptr->data, NULL, &dst_rect);

  return self;
}

void
Init_red_bird_text(void)
{
  bird_cText = rb_define_class_under(bird_m, "Text", rb_cObject);
  rb_define_alloc_func(bird_cText, bird_alloc_text);
  rb_define_method(bird_cText, "initialize", bird_cText_initialize, 4);
  rb_define_method(bird_cText, "width", bird_cText_width, 0);
  rb_define_method(bird_cText, "height", bird_cText_height, 0);
  rb_define_method(bird_cText, "render_to_screen",
                   bird_cText_render_to_screen, 2);
}
