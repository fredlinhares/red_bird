// SPDX-License-Identifier: MIT
#include "color.h"

/*
  Document-class: RedBird::Color
  A color represented by red, green, blue, and alpha (transparency) values.
*/
VALUE bird_cColor;

/*
  Basic functions all Ruby classes need.
 */

void
bird_free_color(void* obj)
{
  struct bird_color_data *ptr = obj;

  free(ptr);
}

size_t
bird_memsize_color(const void* obj)
{
  // TODO
  return 0;
}

static const rb_data_type_t
bird_color_type = {
  "red_bird_color",
  {0, bird_free_color, bird_memsize_color,},
  0, 0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

VALUE
bird_alloc_color(VALUE klass)
{
  VALUE obj;
  struct bird_color_data *ptr;

  obj = TypedData_Make_Struct(klass, struct bird_color_data, &bird_color_type,
                              ptr);

  return obj;
}

/*
  Create a new color based in the RGBA color model.

  @param red [Integer] the amount of red (between 0 and 255).
  @param green [Integer] the amount of green (between 0 and 255).
  @param blue [Integer] the amount of blue (between 0 and 255).
  @param alpha [Integer] the amount of transparency (between 0 and 255). 0 is
    transparent, 255 is opaque.
*/
VALUE
bird_cColor_initialize(VALUE self, VALUE red, VALUE green, VALUE blue,
                       VALUE alpha)
{
  struct bird_color_data *ptr;

  RB_INTEGER_TYPE_P(red);
  RB_INTEGER_TYPE_P(green);
  RB_INTEGER_TYPE_P(blue);
  RB_INTEGER_TYPE_P(alpha);

  TypedData_Get_Struct(self, struct bird_color_data, &bird_color_type, ptr);

  ptr->data.r = NUM2UINT(red);
  ptr->data.g = NUM2UINT(green);
  ptr->data.b = NUM2UINT(blue);
  ptr->data.a = NUM2UINT(alpha);

  return self;
}

struct bird_color_data*
bird_cColor_get_data(VALUE self)
{
  struct bird_color_data *ptr;

  TypedData_Get_Struct(self, struct bird_color_data, &bird_color_type, ptr);

  return ptr;
}

void
Init_red_bird_color(void)
{
  bird_cColor = rb_define_class_under(bird_m, "Color", rb_cObject);
  rb_define_alloc_func(bird_cColor, bird_alloc_color);
  rb_define_method(bird_cColor, "initialize", bird_cColor_initialize, 4);
}
