// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_LOADER_H
#define RED_BIRD_LOADER_H 1

#include "main.h"

struct LoaderStack_s;

typedef struct
{
  SDL_bool (*load)(void *obj, struct LoaderStack_s *ls);
  void (*unload)(void *obj, struct LoaderStack_s *ls);
} Loader;

struct LoaderStack_s
{
  void *obj;
  SDL_bool is_loaded;
  int32_t last_loaded;

  uint32_t loader_capacity;
  uint32_t loader_size;
  Loader *loaders;

  char* error_message;
};

typedef struct LoaderStack_s LoaderStack;

void
LoaderStack_constructor(LoaderStack *self, void *obj);

void
LoaderStack_destructor(LoaderStack *self);

void
LoaderStack_add(LoaderStack *self,
  SDL_bool (*load)(void *obj, LoaderStack *ls),
  void (*unload)(void *obj, LoaderStack *ls));

void
LoaderStack_set_error(LoaderStack *self, const char* base_error,
  const char* current_error);

SDL_bool
LoaderStack_load(LoaderStack *self);

SDL_bool
LoaderStack_reload(LoaderStack *self, int32_t step);

void
LoaderStack_unload(LoaderStack *self);

#endif /* RED_BIRD_LOADER_H */
