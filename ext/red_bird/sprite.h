// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_SPRITE_H
#define RED_BIRD_SPRITE_H 1

#include "main.h"

extern VALUE bird_cSprite;

struct bird_sprite_data
{
  SDL_Rect rect;
};

VALUE
bird_alloc_sprite(VALUE klass);

VALUE
bird_cSprite_initialize(VALUE self, VALUE texture, VALUE  x, VALUE  y,
                        VALUE  width, VALUE  height);

VALUE
bird_cSprite_render_to_screen(VALUE self, VALUE  x, VALUE  y);

void
Init_red_bird_sprite(void);

#endif // RED_BIRD_SPRITE_H
