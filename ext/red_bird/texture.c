// SPDX-License-Identifier: MIT
#include "texture.h"
#include "texture_imp.hpp"

#include "engine.h"
#include "palette.h"

/*
  Document-class: RedBird::Texture

  An image optimized for rendering on the screen.

  @author Frederico Linhares
*/
VALUE bird_cTexture;

/*
  Basic functions all Ruby classes need.
 */

void
bird_free_texture(void* obj)
{
  struct bird_texture_data *ptr = obj;

  SDL_DestroyTexture(ptr->data);
  free(ptr);
}

size_t
bird_memsize_texture(const void* obj)
{
  // TODO
  return 0;
}

static const rb_data_type_t
bird_texture_type = {
  "red_bird_texture",
  {0, bird_free_texture, bird_memsize_texture,},
  0, 0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

VALUE
bird_alloc_texture(VALUE klass)
{
  VALUE obj;
  struct bird_texture_data *ptr;

  obj = TypedData_Make_Struct(klass, struct bird_texture_data,
                              &bird_texture_type, ptr);

  return obj;
}

/*
  @param file_path [String] path to an image to be loaded into a texture.
  @author Frederico Linhares
*/
VALUE
bird_cTexture_initialize(VALUE self, VALUE file_path, VALUE palette)
{
  struct bird_texture_data *ptr;
  struct bird_palette_data *palette_data;
  struct bird_cTexture_PGM pgm_img;
  SDL_Surface *surface = NULL;

  if(!engine_initialized)
    rb_raise(rb_eRuntimeError, "%s",
             "can not create a RedBird::Texture instance before "
             "RedBird::Engine is started");

  if(!rb_obj_is_kind_of(palette, bird_cPalette))
    rb_raise(
        rb_eArgError, "%s",
        "palette must be and instance of RedBird::Palette");

  SafeStringValue(file_path);
  if(!bird_cTexture_PGM_load(&pgm_img, StringValueCStr(file_path)))
    rb_raise(
        rb_eArgError, "%s",
        "failed to load image: it does not exists or is in an invalid format");

  // Depth = 8 bits image.
  // Pitch = 1 byte (8 bits) * image width.
  surface = SDL_CreateRGBSurfaceWithFormatFrom(
      pgm_img.data, pgm_img.width, pgm_img.height, 8, pgm_img.width,
      SDL_PIXELFORMAT_INDEX8);
  if(surface == NULL)
    rb_raise(rb_eArgError, "failed to create surface: %s\n", SDL_GetError());

  TypedData_Get_Struct(self, struct bird_texture_data, &bird_texture_type,
                       ptr);
  palette_data = bird_cPalette_get_data(palette);
  SDL_SetPaletteColors(surface->format->palette, palette_data->colors, 0, 256);

  ptr->data = SDL_CreateTextureFromSurface(bird_core.renderer, surface);
  bird_cTexture_PGM_unload(&pgm_img);
  SDL_FreeSurface(surface);
  if(ptr->data == NULL)
    rb_raise(rb_eArgError, "failed to convert image: %s\n", SDL_GetError());

  SDL_QueryTexture(ptr->data, NULL, NULL, &ptr->width, &ptr->height);
  return self;
}

/*
  @return [Integer]
  @author Frederico Linhares
*/
VALUE
bird_cTexture_width(VALUE self)
{
  struct bird_texture_data *ptr;

  TypedData_Get_Struct(self, struct bird_texture_data, &bird_texture_type,
                       ptr);

  return INT2FIX(ptr->width);
}

/*
  @return [Integer]
  @author Frederico Linhares
*/
VALUE
bird_cTexture_height(VALUE self)
{
  struct bird_texture_data *ptr;

  TypedData_Get_Struct(self, struct bird_texture_data, &bird_texture_type,
                       ptr);

  return INT2FIX(ptr->height);
}

struct bird_texture_data*
bird_cTexture_get_data(VALUE self)
{
  struct bird_texture_data *ptr;

  TypedData_Get_Struct(self, struct bird_texture_data, &bird_texture_type,
                       ptr);

  return ptr;
}

void
Init_red_bird_texture(void)
{
  bird_cTexture = rb_define_class_under(bird_m, "Texture", rb_cObject);
  rb_define_alloc_func(bird_cTexture, bird_alloc_texture);
  rb_define_method(bird_cTexture, "initialize", bird_cTexture_initialize, 2);
  rb_define_method(bird_cTexture, "width", bird_cTexture_width, 0);
  rb_define_method(bird_cTexture, "height", bird_cTexture_height, 0);
}
