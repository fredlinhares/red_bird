// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_PALETTE_H
#define RED_BIRD_PALETTE_H 1

#include "main.h"

extern VALUE bird_cPalette;

struct bird_palette_data
{
  SDL_Color colors[256];
};

VALUE
bird_cPalette_initialize(VALUE self, VALUE colors);

struct bird_palette_data*
bird_cPalette_get_data(VALUE self);

void
Init_red_bird_palette(void);

#endif /* RED_BIRD_PALETTE_H */
