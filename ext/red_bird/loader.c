// SPDX-License-Identifier: MIT
#include <stdlib.h>

#include "loader.h"

static SDL_bool
LoaderStack_step_load(LoaderStack *self, int32_t step)
{
  SDL_bool error = SDL_FALSE;

  // Already loaded, nothing to do.
  if(self->last_loaded >= step) return SDL_TRUE;

  for(;self->last_loaded < step; self->last_loaded++)
    {
      error = !self->loaders[self->last_loaded].load(self->obj, self);
      if(error) break;
    }

  // Self number will be one after the last loeaded after the loading loop is
  // over.
  self->last_loaded--;

  if(error)
    {
      // Backward interaction to unload what was loaded.
      for(;self->last_loaded >= 0; self->last_loaded--)
	self->loaders[self->last_loaded].unload(self->obj, self);
      self->last_loaded++;
    }

  return !error;
}

static void
LoaderStack_step_unload(LoaderStack *self, int32_t step)
{
  // Already unloaded, nothing to do.
  if(self->last_loaded <= step) return;

  for(; self->last_loaded >= 0; self->last_loaded--)
    self->loaders[self->last_loaded].unload(self->obj, self);

  // Self number will be one before the last loeaded after the unloading loop
  // is over.
  self->last_loaded++;

  self->is_loaded = SDL_FALSE;
}

void
LoaderStack_constructor(LoaderStack *self, void *obj)
{
  self->is_loaded = SDL_FALSE;
  self->last_loaded = 0;

  self->loader_capacity = 10; // Totally arbitrary value.
  self->loader_size = 0;

  self->obj = obj;

  self->error_message = 0;

  self->loaders = malloc(sizeof(Loader) * self->loader_capacity);
}

void
LoaderStack_destructor(LoaderStack *self)
{
  if(self->error_message != 0)
    free(self->error_message);

  if(self->is_loaded) LoaderStack_unload(self);
  free(self->loaders);
}

void
LoaderStack_add(LoaderStack *self,
  SDL_bool (*load)(void *obj, LoaderStack *ls),
  void (*unload)(void *obj, LoaderStack *ls))
{
  Loader *l;
  uint32_t next_loader = self->loader_size;

  // Expand if is full.
  if(self->loader_size == self->loader_capacity)
    {
      self->loader_capacity += 5; // Totally arbitrary value.
      self->loaders = realloc(self->loaders,
	sizeof(Loader) * self->loader_capacity);
    }

  l = &(self->loaders[next_loader]);
  l->load = load;
  l->unload = unload;

  self->loader_size++;
}

void
LoaderStack_set_error(LoaderStack *self, const char* base_error,
  const char* current_error)
{
  if(self->error_message != 0)
    free(self->error_message);

  if(current_error != NULL)
    {
      self->error_message =
	malloc(strlen(base_error) + strlen(current_error) + 1);

      strcpy(self->error_message, base_error);
      strcat(self->error_message, current_error);
    }
  else
    {
      self->error_message = malloc(strlen(base_error));

      strcpy(self->error_message, base_error);
    }
}

SDL_bool
LoaderStack_load(LoaderStack *self)
{
  if(self->is_loaded) return SDL_TRUE;

  if(LoaderStack_step_load(self, self->loader_size))
    {
      self->is_loaded = SDL_TRUE;
      return SDL_TRUE;
    }
  else
    {
      return SDL_FALSE;
    }
}

SDL_bool
LoaderStack_reload(LoaderStack *self, int32_t step)
{
  if(!self->is_loaded) return SDL_FALSE;

  LoaderStack_step_unload(self, self->loader_size);
  return LoaderStack_step_load(self, step);
}

void
LoaderStack_unload(LoaderStack *self)
{
  if(!self->is_loaded) return;

  LoaderStack_step_unload(self, 0);
}
