// SPDX-License-Identifier: MIT
#include "main.h"

#include "bird.h"
#include "color.h"
#include "dynamic_sprite.h"
#include "engine.h"
#include "font.h"
#include "keycode.h"
#include "input_device.h"
#include "palette.h"
#include "rect.h"
#include "render.h"
#include "sprite.h"
#include "texture.h"
#include "text.h"
#include "timer.h"

VALUE bird_m;

void
Init_red_bird(void)
{
  Init_red_bird_module();
  Init_red_bird_engine();
  Init_red_bird_color();
  Init_red_bird_dynamic_sprite();
  Init_red_bird_font();
  Init_red_bird_keycode();
  Init_red_bird_input_device();
  Init_red_bird_palette();
  Init_red_bird_rect();
  Init_red_bird_render();
  Init_red_bird_sprite();
  Init_red_bird_text();
  Init_red_bird_texture();
  Init_red_bird_timer();
}
