// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_INPUT_DEVICE_H
#define RED_BIRD_INPUT_DEVICE_H 1

#include "main.h"

extern VALUE bird_cInputDevice;

VALUE
bird_cInputDevice_get_events(VALUE self);

void
Init_red_bird_input_device(void);

#endif /* RED_BIRD_INPUT_DEVICE_H */
