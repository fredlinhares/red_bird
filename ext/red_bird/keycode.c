// SPDX-License-Identifier: MIT
#include "keycode.h"

VALUE bird_mKeycode;

void
Init_red_bird_keycode(void)
{
  bird_mKeycode = rb_define_module_under(bird_m, "Keycode");

  rb_define_const(bird_mKeycode, "A", INT2NUM(SDLK_a));
  rb_define_const(bird_mKeycode, "B", INT2NUM(SDLK_b));
  rb_define_const(bird_mKeycode, "C", INT2NUM(SDLK_c));
  rb_define_const(bird_mKeycode, "D", INT2NUM(SDLK_d));
  rb_define_const(bird_mKeycode, "E", INT2NUM(SDLK_e));
  rb_define_const(bird_mKeycode, "F", INT2NUM(SDLK_f));
  rb_define_const(bird_mKeycode, "G", INT2NUM(SDLK_g));
  rb_define_const(bird_mKeycode, "H", INT2NUM(SDLK_h));
  rb_define_const(bird_mKeycode, "I", INT2NUM(SDLK_i));
  rb_define_const(bird_mKeycode, "J", INT2NUM(SDLK_j));
  rb_define_const(bird_mKeycode, "K", INT2NUM(SDLK_k));
  rb_define_const(bird_mKeycode, "L", INT2NUM(SDLK_l));
  rb_define_const(bird_mKeycode, "M", INT2NUM(SDLK_m));
  rb_define_const(bird_mKeycode, "N", INT2NUM(SDLK_n));
  rb_define_const(bird_mKeycode, "O", INT2NUM(SDLK_o));
  rb_define_const(bird_mKeycode, "P", INT2NUM(SDLK_p));
  rb_define_const(bird_mKeycode, "Q", INT2NUM(SDLK_q));
  rb_define_const(bird_mKeycode, "R", INT2NUM(SDLK_r));
  rb_define_const(bird_mKeycode, "S", INT2NUM(SDLK_s));
  rb_define_const(bird_mKeycode, "T", INT2NUM(SDLK_t));
  rb_define_const(bird_mKeycode, "U", INT2NUM(SDLK_u));
  rb_define_const(bird_mKeycode, "V", INT2NUM(SDLK_v));
  rb_define_const(bird_mKeycode, "W", INT2NUM(SDLK_w));
  rb_define_const(bird_mKeycode, "X", INT2NUM(SDLK_x));
  rb_define_const(bird_mKeycode, "Y", INT2NUM(SDLK_y));
  rb_define_const(bird_mKeycode, "Z", INT2NUM(SDLK_z));

  rb_define_const(bird_mKeycode, "KEY_UP", INT2NUM(SDLK_UP));
  rb_define_const(bird_mKeycode, "KEY_DOWN", INT2NUM(SDLK_DOWN));
  rb_define_const(bird_mKeycode, "KEY_LEFT", INT2NUM(SDLK_LEFT));
  rb_define_const(bird_mKeycode, "KEY_RIGHT", INT2NUM(SDLK_RIGHT));
}
