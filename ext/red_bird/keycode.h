// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_KEYCODE_H
#define RED_BIRD_KEYCODE_H 1

#include "main.h"

extern VALUE bird_mKeycode;

void
Init_red_bird_keycode(void);

#endif /* RED_BIRD_KEYCODE_H */
