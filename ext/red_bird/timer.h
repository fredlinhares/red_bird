// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_TIMER_H
#define RED_BIRD_TIMER_H 1

#include "main.h"

extern VALUE bird_cTimer;

struct bird_timer_data
{
  Uint32 frame_start;
  Uint32 max_frame_duration_ms; // in miliseconds
  double max_frame_duration_sec; // in seconds
};

VALUE
bird_cTimer_initialize(VALUE self, VALUE max_fps);

VALUE
bird_cTimer_tick(VALUE self);

void
Init_red_bird_timer(void);

#endif // RED_BIRD_TIMER_H
