// SPDX-License-Identifier: MIT
#include "sprite.h"

#include "engine.h"
#include "texture.h"

VALUE bird_cSprite;

static ID id_at_texture;

/*
  Basic functions that all Ruby classes need.
 */

void
bird_free_sprite(void* obj)
{
  struct bird_sprite_data *ptr = obj;

  free(ptr);
}

size_t
bird_memsize_sprite(const void* obj)
{
  // TODO
  return 0;
}

static const rb_data_type_t
bird_sprite_type = {
  "red_bird_sprite",
  {0, bird_free_sprite, bird_memsize_sprite,},
  0, 0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

VALUE
bird_alloc_sprite(VALUE klass)
{
  VALUE obj;
  struct bird_sprite_data *ptr;

  obj = TypedData_Make_Struct(klass, struct bird_sprite_data,
                              &bird_sprite_type, ptr);

  return obj;
}

/*
  @param texture [RedBird::Texture] image to use as source for this sprite.
  @param x [Integer] vertical position in the texture.
  @param y [Integer] horizontal position in the texture.
  @param width [Integer] width of the sprite.
  @param height [Integer] height of the sprite.
  @return [RedBird::Sprite] self
  @author Frederico Linhares
*/
VALUE
bird_cSprite_initialize(VALUE self, VALUE texture, VALUE  x, VALUE  y,
                        VALUE  width, VALUE  height)
{
  struct bird_sprite_data *ptr;

  if(!rb_obj_is_kind_of(texture, bird_cTexture))
    rb_raise(rb_eTypeError, "%s",
             "texture must be an instance of RedBird::Texture");
  RB_INTEGER_TYPE_P(x);
  RB_INTEGER_TYPE_P(y);
  RB_INTEGER_TYPE_P(width);
  RB_INTEGER_TYPE_P(height);

  TypedData_Get_Struct(self, struct bird_sprite_data, &bird_sprite_type, ptr);

  rb_ivar_set(self, id_at_texture, texture);
  ptr->rect.x = NUM2INT(x);
  ptr->rect.y = NUM2INT(y);
  ptr->rect.w = NUM2INT(width);
  ptr->rect.h = NUM2INT(height);

  return self;
}

/*
  Render this sprite to the screen.

  @param x [Integer] screen horizontal position.
  @param y [Integer] screen vertical position.
  @return [RedBird::Sprite] self
  @author Frederico Linhares
*/
VALUE
bird_cSprite_render_to_screen(VALUE self, VALUE  x, VALUE  y)
{
  SDL_Rect dst_rect;

  struct bird_sprite_data *ptr;
  struct bird_texture_data *texture_ptr;

  VALUE texture;

  RB_INTEGER_TYPE_P(x);
  RB_INTEGER_TYPE_P(y);

  texture = rb_ivar_get(self, id_at_texture);

  TypedData_Get_Struct(self, struct bird_sprite_data, &bird_sprite_type, ptr);
  texture_ptr = bird_cTexture_get_data(texture);

  dst_rect.x = NUM2INT(x);
  dst_rect.y = NUM2INT(y);
  dst_rect.w = ptr->rect.w;
  dst_rect.h = ptr->rect.h;

  SDL_RenderCopy(bird_core.renderer, texture_ptr->data, &ptr->rect,
                 &dst_rect);

  return self;
}

void
Init_red_bird_sprite(void)
{
  id_at_texture = rb_intern("@texture");
  bird_cSprite = rb_define_class_under(bird_m, "Sprite", rb_cObject);
  rb_define_alloc_func(bird_cSprite, bird_alloc_sprite);
  rb_define_method(bird_cSprite, "initialize", bird_cSprite_initialize, 5);
  rb_define_method(bird_cSprite, "render_to_screen",
                   bird_cSprite_render_to_screen, 2);
}
