// SPDX-License-Identifier: MIT
#include "render.h"

#include "color.h"
#include "engine.h"

/*
  Document-module: RedBird::Render

  This module contains basic rendering functionalities.

  @author Frederico Linhares
*/
VALUE bird_mRender;


/*
  Use this method to define the color that will be used by some drawing
  methods.

  @param color [RedBird::Color]
  @return [RedBird::Render] self
  @author Frederico Linhares
*/
VALUE
bird_mRender_set_color(VALUE self, VALUE color)
{
  struct bird_color_data *color_ptr;

  if(!rb_obj_is_kind_of(color, bird_cColor))
  {
    rb_raise(rb_eTypeError, "%s",
             "color must be an instance of RedBird::Color");

    return self;
  }

  color_ptr = bird_cColor_get_data(color);

  SDL_SetRenderDrawColor(
      bird_core.renderer, color_ptr->data.r, color_ptr->data.g,
      color_ptr->data.b, color_ptr->data.a);

  return self;
}

/*
  Draw a rectangle filled with a color defined by RedBird::Render.set_color.

  @param x [Integer]
  @param y [Integer]
  @param width [Integer]
  @param height [Integer]
  @return [RedBird::Render] self
  @see RedBird::Render.set_color
  @author Frederico Linhares
*/
VALUE
bird_mRender_fill_rect(VALUE self, VALUE x, VALUE y, VALUE width, VALUE height)
{
  SDL_Rect rect;

  RB_INTEGER_TYPE_P(x);
  RB_INTEGER_TYPE_P(y);
  RB_INTEGER_TYPE_P(width);
  RB_INTEGER_TYPE_P(height);

  rect.x = FIX2INT(x);
  rect.y = FIX2INT(y);
  rect.w = FIX2INT(width);
  rect.h = FIX2INT(height);

  SDL_RenderFillRect(bird_core.renderer, &rect);

  return self;
}

/*
  Fill the entire screen with a single color defined by
  RedBird::Render.set_color.

  @return [RedBird::Render] self
  @see RedBird::Render.set_color
  @author Frederico Linhares
*/
VALUE
bird_mRender_clear_screen(VALUE self)
{
  SDL_RenderClear(bird_core.renderer);

  return self;
}

/*
  Every time you draw anything, this drawing is not applied directly to the
  screen; instead, the command draws to a buffer. This procedure allows you to
  do all the drawing before you present it to the user. After you finish all
  updates you want into the buffer, you must call this method to send the final
  buffer to the screen.

  @return [RedBird::Render] self
  @author Frederico Linhares
*/
VALUE
bird_mRender_update_screen(VALUE self)
{
  SDL_SetRenderTarget(bird_core.renderer, NULL);

  SDL_RenderCopy(bird_core.renderer, bird_core.pre_screen_buffer, NULL,
                 &bird_core.screen_rect);
  SDL_RenderPresent(bird_core.renderer);

  SDL_SetRenderTarget(bird_core.renderer, bird_core.pre_screen_buffer);

  return self;
}

void
Init_red_bird_render(void)
{
  bird_mRender = rb_define_module_under(bird_m, "Render");
  rb_define_module_function(bird_mRender, "set_color", bird_mRender_set_color,
                            1);
  rb_define_module_function(bird_mRender, "fill_rect", bird_mRender_fill_rect,
                            4);
  rb_define_module_function(bird_mRender, "clear_screen",
                            bird_mRender_clear_screen, 0);
  rb_define_module_function(bird_mRender, "update_screen",
                            bird_mRender_update_screen, 0);
}
