// SPDX-License-Identifier: MIT
#include "texture_imp.hpp"

#include <charconv>
#include <fstream>
#include <string>

SDL_bool
bird_cTexture_PGM_load(
    struct bird_cTexture_PGM *pgm_img, const char *file_path)
{
  int wh_pos;
  std::string line;
  std::ifstream file(file_path);

  if(!file) return SDL_FALSE;

  // Read file magic.
  std::getline(file, line);
  if(line != "P5") return SDL_FALSE;

  // Read file comment.
  std::getline(file, line);

  // Read file width and height.
  std::getline(file, line);
  wh_pos = line.find(" ");
  std::from_chars(line.data(), line.data() + wh_pos, pgm_img->width);
  std::from_chars(
      line.data() + wh_pos + 1, line.data() + line.size(), pgm_img->height);

  // Read file maximum value.
  std::getline(file, line);
  std::from_chars(line.data(), line.data() + line.size(), pgm_img->max_value);

  // Read file values.
  pgm_img->data_size = pgm_img->width * pgm_img->height;
  line.reserve(pgm_img->data_size);
  file.read(line.data(), pgm_img->data_size);
  pgm_img->data = new char[pgm_img->data_size];
  memcpy(pgm_img->data, line.data(), pgm_img->data_size);

  return SDL_TRUE;
}

void
bird_cTexture_PGM_unload(bird_cTexture_PGM *pgm_img)
{
  delete[] pgm_img->data;
}
