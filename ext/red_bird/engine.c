// SPDX-License-Identifier: MIT
#include "engine.h"

#include "input_device.h"

VALUE bird_mEngine;
SDL_bool engine_initialized;

/*
  Document-module: RedBird::Engine

  This module is responsible for the initialization, control of general
  behavior, and finalization of a game.
  @author Frederico Linhares
*/
bird_sCore bird_core;

/*
  Stretches the image to fill the screen vertically, horizontally, or both;
  without changing the game aspect ratio.
 */
static void
calculate_full_scale()
{
  double screen_ratio, game_ratio, scale;

  screen_ratio =
      (double)bird_core.screen_width/(double)bird_core.screen_height;
  game_ratio = (double)bird_core.game_width/(double)bird_core.game_height;

  // If screen is proportionally taller than game.
  if(screen_ratio < game_ratio)
  {
    scale = (double)bird_core.screen_width/(double)bird_core.game_width;

    bird_core.screen_rect.w = bird_core.game_width * scale;
    bird_core.screen_rect.h = bird_core.game_height * scale;
    bird_core.screen_rect.x = 0;
    bird_core.screen_rect.y = bird_core.screen_height/2 -
                              bird_core.screen_rect.h/2;
  }
  // If screen is proportionally wider than game.
  else if(screen_ratio > game_ratio)
  {
    scale = (double)bird_core.screen_height/(double)bird_core.game_height;

    bird_core.screen_rect.w = bird_core.game_width * scale;
    bird_core.screen_rect.h = bird_core.game_height * scale;
    bird_core.screen_rect.x = bird_core.screen_width/2 -
                              bird_core.screen_rect.w/2;
    bird_core.screen_rect.y = 0;
  }
  // If they have the same aspect ratio.
  else
  {
    bird_core.screen_rect.x = 0;
    bird_core.screen_rect.y = 0;
    bird_core.screen_rect.w = bird_core.screen_width;
    bird_core.screen_rect.h = bird_core.screen_height;
  }
}

static SDL_bool
load_variables(void *obj, LoaderStack *ls)
{
  // TODO: make user define those variables instead of hard coded.
  bird_core.game_name = "Red Bird Game";

  return SDL_TRUE;
}

static void
unload_variables(void *obj, LoaderStack *ls)
{
}

static SDL_bool
load_sdl(void *obj, LoaderStack *ls)
{
  if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
  {
    const char* base_error = "SDL could not initialize! SDL Error → ";
    LoaderStack_set_error(ls, base_error, SDL_GetError());
    return SDL_FALSE;
  }

  return SDL_TRUE;
}

static void
unload_sdl(void *obj, LoaderStack *ls)
{
  SDL_Quit();
}

static SDL_bool
load_window(void *obj, LoaderStack *ls)
{
  bird_core.window = NULL;
  bird_core.window = SDL_CreateWindow(
    bird_core.game_name, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
    bird_core.screen_width, bird_core.screen_height, SDL_WINDOW_SHOWN);
  if(bird_core.window == NULL)
  {
    const char* base_error = "Window could not be created! SDL_Error → ";
    LoaderStack_set_error(ls, base_error, SDL_GetError());
    return SDL_FALSE;
  }

  return SDL_TRUE;
}

static void
unload_window(void *obj, LoaderStack *ls)
{
  SDL_DestroyWindow(bird_core.window);
}


static SDL_bool
load_sdl_renderer(void *obj, LoaderStack *ls)
{
  bird_core.renderer = NULL;
  bird_core.renderer = SDL_CreateRenderer(
      bird_core.window, -1, SDL_RENDERER_ACCELERATED |
      SDL_RENDERER_TARGETTEXTURE);
  if(bird_core.renderer == NULL)
  {
    const char* base_error = "Could not create SDL renderer → ";
    LoaderStack_set_error(ls, base_error, SDL_GetError());
    return SDL_FALSE;
  }

  return SDL_TRUE;
}

static void
unload_sdl_renderer(void *obj, LoaderStack *ls)
{
  SDL_DestroyRenderer(bird_core.renderer);
}

static SDL_bool
load_pre_screen(void *obj, LoaderStack *ls)
{
  bird_core.pre_screen_buffer = SDL_CreateTexture(
      bird_core.renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
      bird_core.screen_width, bird_core.screen_height);

  if(bird_core.pre_screen_buffer == NULL)
  {
    const char* base_error = "Could not create renderering buffer → ";
    LoaderStack_set_error(ls, base_error, SDL_GetError());
    return SDL_FALSE;
  }

  calculate_full_scale();

  SDL_SetRenderTarget(bird_core.renderer, bird_core.pre_screen_buffer);

  return SDL_TRUE;
}

static void
unload_pre_screen(void *obj, LoaderStack *ls)
{
  SDL_DestroyTexture(bird_core.pre_screen_buffer);
}

static SDL_bool
load_sdl_ttf(void *obj, LoaderStack *ls)
{
  if(TTF_Init())
  {
    const char* base_error = "Could not initialize SDL ttf → ";
    LoaderStack_set_error(ls, base_error, TTF_GetError());
    return SDL_FALSE;
  }

  return SDL_TRUE;
}

static void
unload_sdl_ttf(void *obj, LoaderStack *ls)
{
  TTF_Quit();
}

/*
  Defines the virtual number of pixels used by the game. When the real screen
  resolution is higher than this, the frames are scaled.

  @param width [Integer] number of columns.
  @param height [Integer] number of rows.
  @author Frederico Linhares
*/

static VALUE
bird_mEngine_set_pixel_quantity(VALUE self, VALUE width, VALUE height)
{
  RB_INTEGER_TYPE_P(width);
  RB_INTEGER_TYPE_P(height);

  bird_core.game_width = FIX2INT(width);
  bird_core.game_height = FIX2INT(height);

  calculate_full_scale();

  return self;
}

/*
  Use this method to define the game screen resolution. By default the
  resolution is set to 480x320.

  @param width [Integer] screen width.
  @param height [Integer] screen height.
  @author Frederico Linhares
*/
static VALUE
bird_mEngine_set_screen_resolution(VALUE self, VALUE width, VALUE height)
{
  // Variables converted to C.
  int c_width, c_height;

  RB_INTEGER_TYPE_P(width);
  RB_INTEGER_TYPE_P(height);

  c_width = FIX2INT(width);
  c_height = FIX2INT(height);

  if (c_width < bird_core.game_width)
    rb_raise(rb_eArgError, "width must be bigger or equal to pixels width");
  if (c_height < bird_core.game_height)
    rb_raise(rb_eArgError, "height must be bigger or equal to pixels height");

  bird_core.screen_width = c_width;
  bird_core.screen_height = c_height;

  calculate_full_scale();

  // If window was already created, this is a resize.
  if(engine_initialized)
    SDL_SetWindowSize(
        bird_core.window, bird_core.screen_width, bird_core.screen_height);

  return self;
}

/*
  By default, debug is false. If you set this to true, the engine displays some
  information useful for debugging your project.

  @param debug [Boolean] true turn bebug on.
  @author Frederico Linhares
*/
static VALUE
bird_mEngine_set_debug(VALUE self, VALUE debug)
{
  switch (TYPE(debug))
  {
    case T_TRUE:
      bird_core.debug = SDL_TRUE;
      break;
    case T_FALSE:
      bird_core.debug = SDL_FALSE;
      break;
    default:
      rb_raise(rb_eTypeError, "debug must be true or false");
      break;
  }

  return self;
}

/*
  It is only necessary to use this method in a game to change the default
  behavior of the Engine. The {Engine.run} alredy uses it in a proper way, so
  calling that method is enough for most games.

  This method loads internal subsystems; most functionalities in this Engine
  depend on those subsystems, so it is necessary to call this method before
  instantiating any class from RedBird. This method must receive a block. After
  the block finishes executing, it unloads all subsystems.

  @author Frederico Linhares
*/
VALUE
bird_mEngine_load(VALUE self)
{
  VALUE result;

  rb_need_block();

  bird_core.loader = malloc(sizeof(LoaderStack));

  LoaderStack_constructor(bird_core.loader, NULL);

  LoaderStack_add(bird_core.loader, &load_variables, &unload_variables);
  LoaderStack_add(bird_core.loader, &load_sdl, &unload_sdl);
  LoaderStack_add(bird_core.loader, &load_window, &unload_window);
  LoaderStack_add(bird_core.loader, &load_sdl_renderer, &unload_sdl_renderer);
  LoaderStack_add(bird_core.loader, &load_pre_screen, &unload_pre_screen);
  LoaderStack_add(bird_core.loader, &load_sdl_ttf, &unload_sdl_ttf);

  // Load Engine
  if(!LoaderStack_load(bird_core.loader))
  {
    rb_raise(rb_eRuntimeError, "%s", bird_core.loader->error_message);
    LoaderStack_destructor(bird_core.loader);

    free(bird_core.loader);

    return self;
  }

  engine_initialized = SDL_TRUE;

  // Execute block
  result = rb_yield(Qundef);

  // Ensure that objects using SDL are destroyed in case of errors.
  // FIXME: This is only a workaround since I do not know how to ensure that
  // objects like SDL_Texture are deallocated before SDL_Quit is called.
  rb_gc_mark(result);
  rb_gc();
  SDL_Delay(1000); // Wait one second to give garbage collector some time.

  // Unload Engine
  LoaderStack_destructor(bird_core.loader);

  free(bird_core.loader);

  engine_initialized = SDL_FALSE;
  return self;
}

void
Init_red_bird_engine(void)
{
  engine_initialized = SDL_FALSE;

  bird_core.debug = SDL_FALSE;
  bird_core.screen_width = 480;
  bird_core.screen_height = 320;
  bird_core.game_width = bird_core.screen_width;
  bird_core.game_height = bird_core.screen_height;

  bird_mEngine = rb_define_module_under(bird_m, "Engine");
  rb_define_module_function(bird_mEngine, "set_pixel_quantity",
                            bird_mEngine_set_pixel_quantity, 2);
  rb_define_module_function(bird_mEngine, "set_screen_resolution",
                            bird_mEngine_set_screen_resolution, 2);
  rb_define_module_function(bird_mEngine, "debug=", bird_mEngine_set_debug, 1);
  rb_define_module_function(bird_mEngine, "load", bird_mEngine_load, 0);
}
