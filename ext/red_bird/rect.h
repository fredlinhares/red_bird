// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_RECT_H
#define RED_BIRD_RECT_H 1

#include "main.h"

extern VALUE bird_cRect;

struct bird_rect_data
{
  SDL_Rect data;
};

VALUE
bird_cRect_initialize(VALUE self, VALUE x, VALUE y, VALUE width, VALUE height);

void
Init_red_bird_rect(void);

#endif /* RED_BIRD_RECT_H */
