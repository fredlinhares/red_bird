// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_RENDER_H
#define RED_BIRD_RENDER_H 1

#include "main.h"

extern VALUE bird_mRender;

VALUE
bird_mRender_set_color(VALUE self, VALUE color);

VALUE
bird_mRender_fill_rect(VALUE self, VALUE x, VALUE y,
                       VALUE width, VALUE height);

VALUE
bird_mRender_clear_screen(VALUE self);

VALUE
bird_mRender_update_screen(VALUE self);

void
Init_red_bird_render(void);

#endif /* RED_BIRD_RENDER_H */
