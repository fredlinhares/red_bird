// SPDX-License-Identifier: MIT
#include "input_device.h"

#include "engine.h"

VALUE bird_cInputDevice;

static VALUE sym_key_down;
static VALUE sym_key_up;
static VALUE sym_mouse_motion;
static VALUE sym_mouse_button_down;
static VALUE sym_mouse_button_up;
static VALUE sym_quit_game;
static VALUE sym_x;
static VALUE sym_y;

VALUE
bird_cInputDevice_get_events(VALUE self)
{
  SDL_Event event;
  int mouse_x;
  int mouse_y;

  VALUE type;
  VALUE data;
  VALUE x;
  VALUE y;

  rb_need_block();

  // Get input
  while(SDL_PollEvent(&event) != 0)
  {
    switch(event.type)
    {
      case SDL_KEYDOWN:
        type = sym_key_down;
        data = INT2NUM(event.key.keysym.sym);
        break;
      case SDL_KEYUP:
        type = sym_key_up;
        data = INT2NUM(event.key.keysym.sym);
        break;
      case SDL_MOUSEMOTION:
        type = sym_mouse_motion;
        SDL_GetMouseState(&mouse_x, &mouse_y);
        x = INT2NUM(mouse_x);
        y = INT2NUM(mouse_y);
        data = rb_hash_new();
        rb_hash_aset(data, sym_x, x);
        rb_hash_aset(data, sym_y, y);
        break;
      case SDL_MOUSEBUTTONDOWN:
        type = sym_mouse_button_down;
        SDL_GetMouseState(&mouse_x, &mouse_y);
        x = INT2NUM(mouse_x);
        y = INT2NUM(mouse_y);
        data = rb_hash_new();
        rb_hash_aset(data, sym_x, x);
        rb_hash_aset(data, sym_y, y);
        break;
      case SDL_MOUSEBUTTONUP:
        type = sym_mouse_button_up;
        SDL_GetMouseState(&mouse_x, &mouse_y);
        x = INT2NUM(mouse_x);
        y = INT2NUM(mouse_y);
        data = rb_hash_new();
        rb_hash_aset(data, sym_x, x);
        rb_hash_aset(data, sym_y, y);
        break;
      case SDL_QUIT:
        type = sym_quit_game;
        data = Qnil;
        break;
      default:
        continue;
    }

    rb_yield_values(2, type, data);
  }

  return self;
}

void
Init_red_bird_input_device(void)
{
  sym_key_down = ID2SYM(rb_intern("key_down"));
  sym_key_up = ID2SYM(rb_intern("key_up"));
  sym_mouse_motion = ID2SYM(rb_intern("mouse_motion"));
  sym_mouse_button_down = ID2SYM(rb_intern("mouse_button_down"));
  sym_mouse_button_up = ID2SYM(rb_intern("mouse_button_up"));
  sym_quit_game = ID2SYM(rb_intern("quit_game"));
  sym_x = ID2SYM(rb_intern("x"));
  sym_y = ID2SYM(rb_intern("y"));

  bird_cInputDevice = rb_define_class_under(bird_m, "InputDevice", rb_cObject);
  rb_define_protected_method(bird_cInputDevice, "events",
                             bird_cInputDevice_get_events, 0);
}
