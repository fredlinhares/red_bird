# SPDX-License-Identifier: MIT
require "mkmf"

$CXXFLAGS += " -std=gnu++17 "

libs = %w{SDL2 SDL2_ttf}
libs.each {|lib| have_library(lib)}

create_makefile("red_bird/red_bird")
