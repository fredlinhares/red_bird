// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_TEXTURE_IMP_H
#define RED_BIRD_TEXTURE_IMP_H 1

#include "main.h"

#ifdef __cplusplus
extern "C"
{
#endif

struct bird_cTexture_PGM
{
  int width, height, max_value, data_size;
  char *data;
};

SDL_bool
bird_cTexture_PGM_load(
    struct bird_cTexture_PGM *pgm_img, const char *file_path);
  
void
bird_cTexture_PGM_unload(struct bird_cTexture_PGM *pgm_img);

#ifdef __cplusplus
}
#endif

#endif /* RED_BIRD_TEXTURE_IMP_H */
