// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_COLOR_H
#define RED_BIRD_COLOR_H 1

#include "main.h"

extern VALUE bird_cColor;

struct bird_color_data
{
  SDL_Color data;
};

VALUE
bird_alloc_color(VALUE klass);

VALUE
bird_cColor_initialize(VALUE self, VALUE red, VALUE green, VALUE blue,
                       VALUE aplha);

struct bird_color_data*
bird_cColor_get_data(VALUE self);

void
Init_red_bird_color(void);

#endif /* RED_BIRD_COLOR_H */
