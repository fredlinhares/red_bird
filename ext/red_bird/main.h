// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_MAIN_H
#define RED_BIRD_MAIN_H 1

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "ruby.h"

extern VALUE bird_m;

#endif /* RED_BIRD_MAIN_H */
