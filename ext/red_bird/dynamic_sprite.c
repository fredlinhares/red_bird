// SPDX-License-Identifier: MIT
#include "dynamic_sprite.h"

#include "engine.h"
#include "texture.h"

VALUE bird_cDynamicSprite;

static ID id_at_texture;
static VALUE id_flip;
static VALUE id_vertical;
static VALUE id_horizontal;
static VALUE id_both;

// TODO: make this class inherit from RedBird::Sprite.

/*
  Basic functions that all Ruby classes need.
 */

void
bird_free_dynamic_sprite(void* obj)
{
  struct bird_dynamic_sprite_data *ptr = obj;

  free(ptr);
}

size_t
bird_memsize_dynamic_sprite(const void* obj)
{
  // TODO
  return 0;
}

static const rb_data_type_t
bird_dynamic_sprite_type = {
  "red_bird_dynamic_sprite",
  {0, bird_free_dynamic_sprite, bird_memsize_dynamic_sprite,},
  0, 0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

VALUE
bird_alloc_dynamic_sprite(VALUE klass)
{
  VALUE obj;
  struct bird_dynamic_sprite_data *ptr;

  obj = TypedData_Make_Struct(klass, struct bird_dynamic_sprite_data,
                              &bird_dynamic_sprite_type, ptr);

  return obj;
}

/*
  @param texture [RedBird::Texture]
  @param x [Integer]
  @param y [Integer]
  @param width [Integer]
  @param height [Integer]
  @param mods [Hash]
*/
VALUE
bird_cDynamicSprite_initialize(VALUE self, VALUE texture, VALUE  x, VALUE  y,
                               VALUE  width, VALUE  height, VALUE mods)
{
  struct bird_dynamic_sprite_data *ptr;

  VALUE hash_val;

  if(!rb_obj_is_kind_of(texture, bird_cTexture))
    rb_raise(rb_eTypeError, "%s",
             "texture must be an instance of RedBird::Texture");
  RB_INTEGER_TYPE_P(x);
  RB_INTEGER_TYPE_P(y);
  RB_INTEGER_TYPE_P(width);
  RB_INTEGER_TYPE_P(height);

  TypedData_Get_Struct(self, struct bird_dynamic_sprite_data,
                       &bird_dynamic_sprite_type, ptr);

  rb_ivar_set(self, id_at_texture, texture);
  ptr->rect.x = NUM2INT(x);
  ptr->rect.y = NUM2INT(y);
  ptr->rect.w = NUM2INT(width);
  ptr->rect.h = NUM2INT(height);

  ptr->angle = 0;
  ptr->center.x = 0;
  ptr->center.y = 0;

  // Get 'flip'.
  ptr->flip = SDL_FLIP_NONE; // By default do not flip.
  hash_val = rb_hash_aref(mods, id_flip);
  if(SYMBOL_P(hash_val))
  {
    if(hash_val == id_vertical)
      ptr->flip = SDL_FLIP_VERTICAL;
    else if(hash_val == id_horizontal)
      ptr->flip = SDL_FLIP_HORIZONTAL;
    else if(hash_val == id_both)
      ptr->flip = SDL_FLIP_VERTICAL | SDL_FLIP_HORIZONTAL;
  }

  return self;
}

/*
  Render itself directly to the screen.

  @param x [Integer] x coordinate of the screen.
  @param y [Integer] y coordinate of the screen.
  @return [RedBird::DynamicSprite] self
  @author Frederico Linhares
*/
VALUE
bird_cDynamicSprite_render_to_screen(VALUE self, VALUE  x, VALUE  y)
{
  SDL_Rect dst_rect;

  struct bird_dynamic_sprite_data *ptr;
  struct bird_texture_data *texture_ptr;

  VALUE texture;

  RB_INTEGER_TYPE_P(x);
  RB_INTEGER_TYPE_P(y);

  texture = rb_ivar_get(self, id_at_texture);

  TypedData_Get_Struct(self, struct bird_dynamic_sprite_data,
                       &bird_dynamic_sprite_type, ptr);
  texture_ptr = bird_cTexture_get_data(texture);

  dst_rect.x = NUM2INT(x);
  dst_rect.y = NUM2INT(y);
  dst_rect.w = ptr->rect.w;
  dst_rect.h = ptr->rect.h;

  SDL_RenderCopyEx(bird_core.renderer, texture_ptr->data, &ptr->rect,
                   &dst_rect, ptr->angle, &ptr->center, ptr->flip);

  return self;
}

void
Init_red_bird_dynamic_sprite(void)
{
  id_at_texture = rb_intern("@texture");
  id_flip = ID2SYM(rb_intern("flip"));
  id_vertical = ID2SYM(rb_intern("vertical"));
  id_horizontal = ID2SYM(rb_intern("horizontal"));
  id_both = ID2SYM(rb_intern("both"));

  bird_cDynamicSprite = rb_define_class_under(bird_m, "DynamicSprite",
                                              rb_cObject);
  rb_define_alloc_func(bird_cDynamicSprite, bird_alloc_dynamic_sprite);
  rb_define_method(bird_cDynamicSprite, "initialize",
                   bird_cDynamicSprite_initialize, 6);
  rb_define_method(bird_cDynamicSprite, "render_to_screen",
                   bird_cDynamicSprite_render_to_screen, 2);
}
