// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_FONT_H
#define RED_BIRD_FONT_H 1

#include "main.h"

extern VALUE bird_cFont;

struct bird_font_data
{
  TTF_Font *data;
};

VALUE
bird_alloc_font(VALUE klass);

VALUE
bird_cFont_initialize(VALUE self, VALUE file_path, VALUE size);

struct bird_font_data*
bird_cFont_get_data(VALUE self);

void
Init_red_bird_font(void);

#endif /* RED_BIRD_FONT_H */
