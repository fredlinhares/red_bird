// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_DYNAMIC_SPRITE_H
#define RED_BIRD_DYNAMIC_SPRITE_H 1

#include "main.h"

extern VALUE bird_cDynamicSprite;

struct bird_dynamic_sprite_data
{
  SDL_Rect rect;
  double angle;
  SDL_Point center;
  SDL_RendererFlip flip;
};

VALUE
bird_alloc_dynamic_sprite(VALUE klass);

VALUE
bird_cDynamicSprite_initialize(VALUE self, VALUE texture, VALUE  x, VALUE  y,
                                VALUE  width, VALUE  height, VALUE mods);

VALUE
bird_cDynamicSprite_render_to_screen(VALUE self, VALUE  x, VALUE  y);

void
Init_red_bird_dynamic_sprite(void);

#endif // RED_BIRD_DYNAMIC_SPRITE_H
