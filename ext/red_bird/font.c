// SPDX-License-Identifier: MIT
#include "font.h"

/*
  Document-class: RedBird::Font

  This class represents a font; it is necessary for rendering any text.

  @author Frederico Linhares
*/
VALUE bird_cFont;

/*
  Basic functions all Ruby classes need.
 */

void
bird_free_font(void* obj)
{
  struct bird_font_data *ptr = obj;

  TTF_CloseFont(ptr->data);
  free(ptr);
}

size_t
bird_memsize_font(const void* obj)
{
  // TODO
  return 0;
}

static const rb_data_type_t
bird_font_type = {
  "red_bird_font",
  {0, bird_free_font, bird_memsize_font,},
  0, 0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

VALUE
bird_alloc_font(VALUE klass)
{
  VALUE obj;
  struct bird_font_data *ptr;

  obj = TypedData_Make_Struct(klass, struct bird_font_data, &bird_font_type,
                              ptr);

  return obj;
}

/*
  @param file_path [String] path to file containing the font.
  @param size [Integer] size of text that will be rendered using this font.
  @author Frederico Linhares
*/

VALUE
bird_cFont_initialize(VALUE self, VALUE file_path, VALUE size)
{
  struct bird_font_data *ptr;

  SafeStringValue(file_path);
  RB_INTEGER_TYPE_P(size);

  TypedData_Get_Struct(self, struct bird_font_data, &bird_font_type, ptr);

  ptr->data = TTF_OpenFont(StringValueCStr(file_path), NUM2INT(size));
  if(!ptr->data)
  {
    rb_raise(rb_eArgError, "failed to load font: %s", TTF_GetError());
  }

  return self;
}

struct bird_font_data*
bird_cFont_get_data(VALUE self)
{
  struct bird_font_data *ptr;

  TypedData_Get_Struct(self, struct bird_font_data, &bird_font_type, ptr);

  return ptr;
}

void
Init_red_bird_font(void)
{
  bird_cFont = rb_define_class_under(bird_m, "Font", rb_cObject);
  rb_define_alloc_func(bird_cFont, bird_alloc_font);
  rb_define_method(bird_cFont, "initialize", bird_cFont_initialize, 2);
}
