// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_ENGINE_H
#define RED_BIRD_ENGINE_H 1

#include "loader.h"
#include "main.h"

extern VALUE bird_mEngine;
extern SDL_bool engine_initialized;

typedef struct
{
  LoaderStack *loader;

  SDL_bool debug;
  const char *game_name;

  SDL_Window *window;
  int screen_width;
  int screen_height;

  int game_width;
  int game_height;

  SDL_Renderer *renderer;

  // All rendering goes here before they are moved to the screen.
  SDL_Texture *pre_screen_buffer;
  SDL_Rect screen_rect;
} bird_sCore;

extern bird_sCore bird_core;

VALUE
bird_mEngine_load(VALUE self);

void
Init_red_bird_engine(void);

#endif /* RED_BIRD_ENGINE_H */
