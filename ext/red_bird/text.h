// SPDX-License-Identifier: MIT
#ifndef RED_BIRD_TEXT_H
#define RED_BIRD_TEXT_H 1

#include "main.h"

extern VALUE bird_cText;

struct bird_text_data
{
  SDL_Texture* data;
  int width, height;
};

VALUE
bird_cText_initialize(VALUE self, VALUE text, VALUE font, VALUE tx_color,
                      VALUE bg_color);

VALUE
bird_cText_width(VALUE self);

VALUE
bird_cText_height(VALUE self);

VALUE
bird_cText_render_to_screen(VALUE self, VALUE  x, VALUE  y);

void
Init_red_bird_text(void);

#endif /* RED_BIRD_TEXT_H */
